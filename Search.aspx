﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Search.aspx.cs" Inherits="Search" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>SimLib --- Search Books</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="tablestyle.css" rel="stylesheet" type="text/css" />
<link href="Search.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="gridviewstyle.css" />
    <style type="text/css">
        .style2
        {
            width: 149px;
        }
        .style3
        {
            width: 92px;
        }
        
       
        </style>
</head>

<body>
<form id="form3" runat="server">
	<div id="menu">
		<ul>
			<li class="active"><a href="Home.aspx" title="">Home</a></li>
			<li><a href="#" title="">Search Books</a></li>
			<li><a href="#" title="">Support</a></li>
			<li><a href="#" title="">About Us</a></li>
		</ul>
	</div>
	<div id="logo">
		<%--<h1><a href="#"><font size= "24">S</font>im<font size= "24">L</font>ib ---</a><span 
                class="style1"><a href="#"><font size= "6">Main Account</font></a></span>
        </h1>--%>
        <asp:Image ID="Header" runat="server" AlternateText="SimLib" Height="55px" 
            ImageAlign="Middle" ImageUrl="~/images/simlib (740 x 90).png" 
            BorderStyle="Groove" Width="733px" />
	</div>
<div id="content">
	<div id="sidebar">
		<div id="login" class="boxed">
			<h2 class="title" style="text-align: center">My Profile</h2>
			<div class="content">				
					<fieldset>
                        <asp:Label ID="lblWelcome" runat="server" Text="Welcome" Font-Bold="True" 
                            Font-Names="Algerian" Font-Size="Large" ForeColor="#479AC6" Width="150px"></asp:Label>
					
                        <asp:Label ID="lblName" runat="server" Text="UserName" Font-Bold="False" 
                            Font-Names="Bernard MT Condensed" Font-Size="Small" ForeColor="#FFBD2E" 
                            Width="150px"></asp:Label>
                    <p>
                    </p>
					
                        <asp:HyperLink ID="HLogout" runat="server" Font-Bold="True" 
                            Font-Names="Bell MT" Font-Size="Small" ForeColor="Red" 
                            NavigateUrl="~/Default.aspx" Width="150px">Log Out</asp:HyperLink>
					
					<p>
					
                        &nbsp;</p>
					<p>
                        <asp:HyperLink ID="HEditProfile" runat="server" Font-Bold="True" 
                            Font-Names="Times New Roman" NavigateUrl="~/EditProfile.aspx" Target="_parent" 
                            ToolTip="Edit Your Profile" Width="150px">Edit Profile</asp:HyperLink>
                            </p>
                    
					
					</fieldset>				
			</div>
		</div>
		<div id="updates" class="boxed">
			<h2 class="title" style="text-align: center">Utilities</h2>
			<div class="content">
				<ul>
					<li>
						<h3>
                            <asp:HyperLink ID="HSearch" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" NavigateUrl="~/Search.aspx" 
                                Width="150px" Target="_parent">Search Books</asp:HyperLink>
                        
                            <asp:HyperLink ID="HCheckedInBooks" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" NavigateUrl="~/CheckedIn.aspx" 
                                Target="_parent" Width="150px">CheckedIn Books</asp:HyperLink>
                        </h3>
						<p></p>
					</li>
                    <asp:Panel ID="Panel2" runat="server">
					<li>
						<h3 id="panel1">
                            <asp:HyperLink ID="HManBooks" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" NavigateUrl="~/ManageBooks.aspx" 
                                Target="_parent" Width="150px">Manage Books</asp:HyperLink>
                        
                            <asp:HyperLink ID="HManUsers" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" NavigateUrl="~/ManageUsers.aspx" 
                                Width="150px" Target="_parent">Manage Users</asp:HyperLink>    
                        
                            <asp:HyperLink ID="HManCat" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" 
                                NavigateUrl="~/ManageCategory.aspx" Target="_parent" Width="150px">Manage 
                            Category</asp:HyperLink>
                        
                            <asp:HyperLink ID="HManPub" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" 
                                NavigateUrl="~/ManagePublishers.aspx" Target="_parent" Width="150px">Manage 
                            Publishers</asp:HyperLink>
                            
                            <asp:HyperLink ID="HManAuthors" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" 
                                NavigateUrl="~/ManageAuthors.aspx" Target="_parent" Width="150px">Manage 
                            Authors</asp:HyperLink>
                        
                            <asp:HyperLink ID="HManSuppliers" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" 
                                NavigateUrl="~/ManageSuppliers.aspx" Target="_parent" Width="150px">Manage 
                            Suppliers</asp:HyperLink>
                        </h3>
						<p></p>
						<h3>
                            <asp:HyperLink ID="HIssue" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" NavigateUrl="~/IssueBooks.aspx" 
                                Target="_parent" Width="150px">Issue Books</asp:HyperLink>
						
                            <asp:HyperLink ID="HReturn" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" NavigateUrl="~/ReturnBooks.aspx" 
                                Target="_parent" Width="150px">Return Books</asp:HyperLink>
						</h3>
					</li>
					</asp:Panel>
				</ul>
			</div>
		</div>
	</div>
	<div id="main">
		<div id="welcome" class="post">
			<h2 class="title" 
                style="font-family: Algerian; font-size: x-large">Search Books
            </h2>
			<div class="story">
				<table id="gradient-style" summary="Registration Details">
                    <tbody>
    	                <tr>
        	                <td class="style3">Book Title</td>
                            <td class="style2">
                                <asp:DropDownList ID="lstBook" runat="server" AutoPostBack="True" 
                                    CausesValidation="True" onselectedindexchanged="lstBook_SelectedIndexChanged" 
                                    Width="300px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p></p>
                <asp:GridView ID="gvBook" runat="server"
                    GridLines="None"
                    AllowPaging="True"
                    CssClass="mGrid"
                    PagerStyle-CssClass="pgr"
                    AlternatingRowStyle-CssClass="alt" CaptionAlign="Top" HorizontalAlign="Center">
                    <PagerStyle CssClass="pgr"></PagerStyle>
                    <AlternatingRowStyle CssClass="alt"></AlternatingRowStyle>
                </asp:GridView>
			</div>
		</div>
		<div id="example" class="post">
		</div>
	</div>
</div>

<div id="footer">
	<p id="legal">Copyright © 2010. All Rights Reserved. <%--Designed by <a href="http://www.pinakidey.emurse.com/">
        PINAKI DEY</a>.--%></p>
	<p id="links"><a href="#">Privacy Policy</a> | <a href="license.txt">Terms of Use</a></p>
</div>
</form>
</body>
</html>
