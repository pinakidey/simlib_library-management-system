﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

public partial class ManageSuppliers : System.Web.UI.Page
{
    bool status = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["USER_TYPE"] == null)
        {
            Response.Redirect("Login.aspx", true);
        }
        if (Page.IsPostBack == false)
        {

            if (Session["USER_TYPE"].ToString() == "A")
            {
                HCheckedInBooks.Visible = false;
                lblName.Text = (string)Session["USER_NAME"].ToString();
            }
            else
            {
                Response.Redirect("Login.aspx", true);
                //Panel2.Visible = false;
                //lblName.Text = (string)Session["NAME"].ToString();
            }
            ReadSupplier();
        }

    }
    private void ReadSupplier()
    {
        try
        {
            string sql = "SELECT COUNT(*) FROM SUPPLIER";
            SqlDataReader sdr;
            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);

            sdr.Read();
            int Supp = (int)sdr[0] + 1;
            if (Supp < 10)
            {
                txtSupplier.Text = string.Format("S000{0}", Supp);
            }
            else if (Supp > 10 && Supp < 100)
            {
                txtSupplier.Text = string.Format("S00{0}", Supp);
            }
            else
            {
                txtSupplier.Text = string.Format("S0{0}", Supp);
            }
            sdr.Close();

            try
            {
                sql = "SELECT Supplier_ID FROM Supplier WHERE STATUS = 'A' OR STATUS = 'I'";
                sdr = pro.ReturnMDetails(sql);
                lstid.Items.Clear();
                lstid.Items.Add("");
                while (sdr.Read())
                {
                    lstid.Items.Add(sdr["Supplier_ID"].ToString().Trim());
                }
                sdr.Close();
            }
            catch (Exception nex)
            {
                //throw ex;
                lblEMsg1.Visible = true;
                lblEMsg1.Text = nex.Message;
            }
           
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg.Visible = true;
            lblEMsg.Text = ex.Message;
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        lblEMsg.Visible = false;
        string[] Supp;
        try
        {
            string sql = "SELECT COUNT(*) FROM SUPPLIER WHERE SUPPLIER_NAME = '" + txtSName.Text + "' And Status = 'A'";
            SqlDataReader sdr;
            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);

            sdr.Read();
            int RegNo = (int)sdr[0];
            if (RegNo > 0)
            {
                lblEMsg.Text = "Supplier Name Already Exists.";
                lblEMsg.Visible = true;
                sdr.Close();
                return;
            }
            else
            {
                Supp = new string[15];

                Supp[0] = txtSupplier.Text;
                Supp[1] = txtSName.Text;
                Supp[2] = txtAdd.Text;
                Supp[3] = txtPhone.Text;
                Supp[4] = txtFax.Text;
                Supp[5] = txtemail.Text;
                if (chkStatus.Checked == true)
                {
                    Supp[6] = "A";
                }
                else
                {
                    Supp[7] = "I";
                }

                bool status;
                status = pro.AddSupplier(Supp);

                if (status == false)
                {
                    lblEMsg.Visible = true;
                    lblEMsg.Text = "Supplier Creation Failed";
                }
                lblEMsg.Visible = true;
                lblEMsg.Text = "Supplier Added.";
                ReadSupplier();
                txtSName.Text = "";
                txtAdd.Text = "";
                txtPhone.Text = "";
                txtFax.Text = "";
                txtemail.Text = "";

                ReadSupplier();
            }
        }
        catch (Exception ex)
        {
            lblEMsg.Visible = true;
            lblEMsg.Text = ex.Message;
        }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        txtSName.Text = "";
        txtAdd.Text = "";
        txtPhone.Text = "";
        txtFax.Text = "";
        txtemail.Text = "";
        chkStatus.Checked = false;
        lblEMsg.Visible = false;

        txtSName1.Text = "";
        txtAdd1.Text = "";
        txtPhone1.Text = "";
        txtFax1.Text = "";
        txtemail1.Text = "";
        chkStatus1.Checked = false;
        lblEMsg1.Visible = false;
    }
    protected void lstid_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            status = true;
            if (lstid.SelectedItem.ToString() == "")
            {
                lblEMsg1.Visible = true;
                lblEMsg1.Text = "Please Select the Supplier";
                return;
            }
            string sql = "SELECT Supplier_Name, Address, Phone, Fax, EMail, Status FROM Supplier WHERE Supplier_ID = '" + lstid.SelectedItem.ToString() + "'";
            SqlDataReader sdr;
            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);

            if (sdr.Read() == true)
            {
                txtSName1.Text = sdr["Supplier_Name"].ToString().Trim();
                txtAdd1.Text = sdr["Address"].ToString().Trim();
                txtPhone1.Text = sdr["Phone"].ToString().Trim();
                txtFax1.Text = sdr["Fax"].ToString().Trim();
                txtemail1.Text = sdr["EMail"].ToString().Trim();
                chkStatus1.Checked = true;
            }
            sdr.Close();
            
        }
        catch (Exception ex)
        {
            lblEMsg1.Visible = true;
            lblEMsg1.Text = ex.Message;
        }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        lblEMsg1.Visible = false;
        string[] Supp;
        Profile pro = new Profile();
        try
        {
            if (lstid.SelectedItem.Text == "")
            {
                lblEMsg1.Text = "Please Select Supplier ID";
                lblEMsg1.Visible = true;
            }
            else
            {
                Supp = new string[15];

                Supp[0] = lstid.Text;
                Supp[1] = txtSName1.Text;
                Supp[2] = txtAdd1.Text;
                Supp[3] = txtPhone1.Text;
                Supp[4] = txtFax1.Text;
                Supp[5] = txtemail1.Text;
                if (chkStatus1.Checked == true)
                {
                    Supp[6] = "A";
                }
                else
                {
                    Supp[6] = "I";
                }

                bool status;
                status = pro.UpdateSupplier(Supp);

                if (status == false)
                {
                    lblEMsg1.Visible = true;
                    lblEMsg1.Text = "Supplier Updation Failed";
                }
                lblEMsg1.Visible = true;
                lblEMsg1.Text = "Supplier Updated.";

                lstid.Items.Clear();
                txtSName1.Text = "";
                txtAdd1.Text = "";
                txtPhone1.Text = "";
                txtFax1.Text = "";
                txtemail1.Text = "";
                chkStatus1.Checked = true;
                ReadSupplier();
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg1.Visible = true;
            lblEMsg1.Text = ex.Message;
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        lblEMsg1.Visible = false;
        Profile pro = new Profile();

        try
        {
            if (lstid.SelectedItem.Text == "")
            {
                lblEMsg1.Text = "Please Select Supplier ID";
                lblEMsg1.Visible = true;
            }
            else
            {
                bool status;
                status = pro.DeleteSupplier(lstid.SelectedItem.Text);

                if (status == false)
                {
                    lblEMsg1.Visible = true;
                    lblEMsg1.Text = "Supplier Deletion Failed";
                }
                lblEMsg1.Visible = true;
                lblEMsg1.Text = "Supplier Deleted.";
                lstid.Items.Clear();

                txtSName1.Text = "";
                txtAdd1.Text = "";
                txtPhone1.Text = "";
                txtFax1.Text = "";
                txtemail1.Text = "";
                chkStatus1.Checked = true;
                ReadSupplier();
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg1.Visible = true;
            lblEMsg1.Text = ex.Message;
        }
    }
}
