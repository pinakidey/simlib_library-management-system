﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

public partial class CheckedIn : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["USER_TYPE"] == null)
        {
            Response.Redirect("Login.aspx",true);
        }

        if (Page.IsPostBack == false)
        {
            if (Session["USER_TYPE"].ToString() == "A")
            {
                HCheckedInBooks.Visible = false;

                lblName.Text = (string)Session["USER_NAME"].ToString();
            }
            else
            {

                Panel2.Visible = false;
                //HSMember.Visible = false;
                lblName.Text = (string)Session["NAME"].ToString();
            }
            try
            {

                string connStr = ConfigurationManager.ConnectionStrings["LibraryConn"].ToString();
                SqlConnection conn = new SqlConnection(connStr);

                string sql = "SELECT DISTINCT BT.BOOK_ID AS 'Book ID', BM.BOOK_TITLE AS 'Book Tite' , A.AUTHOR_NAME AS 'Author Name', ";
                sql = sql + "P.PUBLISHER_NAME AS 'Publisher Name', BT.ISSUE_DATE AS 'Issue Date', BT.RETURN_DATE AS 'Return Date', ";
                sql = sql + "BT.ACTUAL_RETURN_DATE AS 'Actual Return Date', BT.LATE_FEE AS 'Late Fee' FROM BOOK_TRANSACTION BT INNER JOIN ";
                sql = sql + "BOOK_MASTER BM ON BT.BOOK_ID = BM.BOOK_ID INNER JOIN AUTHOR A ON BM.AUTHOR_ID = A.AUTHOR_ID ";
                sql = sql + "INNER JOIN PUBLISHER P ON BM.PUBLISHER_ID = P.PUBLISHER_ID WHERE BT.MEMBER_ID = '" + Session["MEMBER_ID"].ToString() + "' AND BT.ACTUAL_RETURN_DATE IS NULL ORDER BY BT.BOOK_ID";
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataAdapter adp = new SqlDataAdapter(cmd);

                //Fill Dataet
                DataSet ds = new DataSet();
                adp.Fill(ds, "Book_Transaction");

                gvBook.DataSource = ds;
                gvBook.DataBind();

                adp.Dispose();
                cmd.Cancel();
                conn.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
