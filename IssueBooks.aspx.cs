﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

public partial class IssueBooks : System.Web.UI.Page
{
    bool status = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["USER_TYPE"] == null)
        {
            Response.Redirect("Login.aspx",true);
        }
        if (Page.IsPostBack == false)
        {
            ReadBook();
            txtIDate.Text = DateTime.Now.ToShortDateString();
            if (Session["USER_TYPE"].ToString() == "A")
            {
                HCheckedInBooks.Visible = false;
                lblName.Text = (string)Session["USER_NAME"].ToString();
            }
            else
            {
                Response.Redirect("Login.aspx", true);
                //Panel2.Visible = false;
                //lblName.Text = (string)Session["NAME"].ToString();
            }
        }
        Calendar1.SelectedDate = DateTime.Today;
        

    }
    private void ReadBook()
    {
        try
        {
            SqlDataReader sdr;
            Profile pro = new Profile();

            string sql = "SELECT Distinct Member_ID FROM Member where Status = 'A'";
            sdr = pro.ReturnMDetails(sql);

            DropDownListRegistration.Items.Add("");
            while (sdr.Read())
            {
                DropDownListRegistration.Items.Add(sdr["Member_ID"].ToString().Trim());
            }
            sdr.Close();

            sql = "SELECT Distinct Book_ID FROM Book_Master where Status = 'A'";
            sdr = pro.ReturnMDetails(sql);

            LstBook.Items.Add("");
            while (sdr.Read())
            {
                LstBook.Items.Add(sdr["Book_ID"].ToString().Trim());
            }
            sdr.Close();

            sql = "SELECT Distinct Book_Title FROM Book_Master where Status = 'A'";
            sdr = pro.ReturnMDetails(sql);

            LstBTitle.Items.Add("");
            while (sdr.Read())
            {
                LstBTitle.Items.Add(sdr["Book_Title"].ToString().Trim());
            }
            sdr.Close();
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg.Visible = true;
            lblEMsg.Text = ex.Message;
        }
    }
    protected void DropDownListRegistration_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblEMsg.Visible = false;
        lblFullName.Visible = false;
        try
        {
            if (DropDownListRegistration.SelectedItem.ToString() == "")
            {
                lblEMsg.Visible = true;
                lblEMsg.Text = "Please Select the Member!";
                return;
            }
            string sql = "SELECT First_Name, Middle_Name, Last_Name FROM Member WHERE Member_ID = '" + DropDownListRegistration.SelectedItem.ToString() + "' And Status = 'A'";
            SqlDataReader sdr;
            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);
            if (sdr.Read() == true)
            {
                lblFullName.Visible = true;
                lblFullName.Text = sdr["First_Name"].ToString().Trim() + " " + sdr["Middle_Name"].ToString().Trim() + " " + sdr["Last_Name"].ToString().Trim();
            }
            sdr.Close();
            return;
        }
        catch (NullReferenceException nex)
        {
            lblEMsg.Visible = true;
            lblEMsg.Text = nex.Message;
            return;
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg.Visible = true;
            lblEMsg.Text = ex.Message;
        }
    }
    protected void LstBook_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblEMsg.Visible = false;
        
        try
        {
            status = true;
            string Author, Publisher, Category, Supplier;
            int AQuantity = 0;

            if (LstBook.SelectedItem.ToString() == "")
            {
                lblEMsg.Visible = true;
                lblEMsg.Text = "Please Select the Book!";
                return;
            }
            string sql = "SELECT Book_Title, Author_ID, Publisher_ID, Category_ID, Supplier_ID, Quantity, Price FROM Book_Master WHERE Book_ID = '" + LstBook.SelectedItem.ToString() + "' And Status = 'A'";
            SqlDataReader sdr;
            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);

            if (sdr.Read() == true)
            {
                //txtBTitle.Text = sdr["Book_Title"].ToString().Trim();
                string BookTitle=sdr["Book_Title"].ToString().Trim();
                LstBTitle.SelectedValue = BookTitle;
                Author = sdr["Author_ID"].ToString().Trim();
                Publisher = sdr["Publisher_ID"].ToString().Trim();
                Category = sdr["Category_ID"].ToString().Trim();
                Supplier = sdr["Supplier_ID"].ToString().Trim();

                AQuantity = Convert.ToInt32(sdr["Quantity"]);
                //if (AQuantity < 1)
                //{
                //    lblDateCompare.Visible = true;
                //}
                txtPrice.Text = sdr["Price"].ToString().Trim();

                //Get Author Name.
                sql = "Select Distinct Author_Name From Author Where Author_ID = '" + Author + "'";
                sdr = pro.ReturnMDetails(sql);
                if (sdr.Read() == true)
                {
                    txtAuthor.Text = sdr["Author_Name"].ToString().Trim();
                }
                
                //Get Publisher_Name
                sql = "Select Distinct Publisher_Name From Publisher Where Publisher_ID = '" + Publisher + "'";
                sdr = pro.ReturnMDetails(sql);
                if (sdr.Read() == true)
                {
                    txtPub.Text = sdr["Publisher_Name"].ToString().Trim();
                }
                
                //Get Category Name
                sql = "Select Distinct Category_Name From Category Where Category_ID = '" + Category + "'";
                sdr = pro.ReturnMDetails(sql);
                if (sdr.Read() == true)
                {
                    txtCat.Text = sdr["Category_Name"].ToString().Trim();
                }
                
                //Get Supplier Name
                sql = "Select Distinct Supplier_Name From Supplier Where Supplier_ID = '" + Supplier + "'";
                sdr = pro.ReturnMDetails(sql);
                if (sdr.Read() == true)
                {
                    txtSup.Text = sdr["Supplier_Name"].ToString().Trim();
                }
                

                sql = "Select Count(1) From Book_Transaction Where Book_ID = '" + LstBook.SelectedItem.Text + "' And Actual_Return_Date Is Null";
                sdr = pro.ReturnMDetails(sql);
                if (sdr.Read() == true)
                {
                    AQuantity -= (int)sdr[0];
                }
            }
            sdr.Close();
            txtQuantity.Text = Convert.ToString(AQuantity);
            return;
        }
        catch (NullReferenceException nex)
        {
            lblEMsg.Visible = true;
            lblEMsg.Text = nex.Message;
            return;
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg.Visible = true;
            lblEMsg.Text = ex.Message;
        }
        
    }
    protected void LstBTitle_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblEMsg.Visible = false;

        try
        {
            status = true;
            string Author, Publisher, Category, Supplier;
            int AQuantity = 0;

            if (LstBook.SelectedItem.ToString() == "")
            {
                lblEMsg.Visible = true;
                lblEMsg.Text = "Please Select the Book!";
                return;
            }
            string sql = "SELECT Book_ID, Author_ID, Publisher_ID, Category_ID, Supplier_ID, Quantity, Price FROM Book_Master WHERE Book_Title = '" + LstBTitle.SelectedItem.ToString().Trim() + "' And Status = 'A'";
            SqlDataReader sdr;
            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);

            if (sdr.Read() == true)
            {

                //txtBTitle.Text = sdr["Book_Title"].ToString().Trim();
                string BookID = sdr["Book_ID"].ToString().Trim();
                LstBook.SelectedValue = BookID;
                Author = sdr["Author_ID"].ToString().Trim();
                Publisher = sdr["Publisher_ID"].ToString().Trim();
                Category = sdr["Category_ID"].ToString().Trim();
                Supplier = sdr["Supplier_ID"].ToString().Trim();

                AQuantity = Convert.ToInt32(sdr["Quantity"]);
                //if (AQuantity < 1)
                //{
                //    lblDateCompare.Visible = true;
                //}
                txtPrice.Text = sdr["Price"].ToString().Trim();

                //Get Author Name.
                sql = "Select Distinct Author_Name From Author Where Author_ID = '" + Author + "'";
                sdr = pro.ReturnMDetails(sql);
                if (sdr.Read() == true)
                {
                    txtAuthor.Text = sdr["Author_Name"].ToString().Trim();
                }

                //Get Publisher_Name
                sql = "Select Distinct Publisher_Name From Publisher Where Publisher_ID = '" + Publisher + "'";
                sdr = pro.ReturnMDetails(sql);
                if (sdr.Read() == true)
                {
                    txtPub.Text = sdr["Publisher_Name"].ToString().Trim();
                }

                //Get Category Name
                sql = "Select Distinct Category_Name From Category Where Category_ID = '" + Category + "'";
                sdr = pro.ReturnMDetails(sql);
                if (sdr.Read() == true)
                {
                    txtCat.Text = sdr["Category_Name"].ToString().Trim();
                }

                //Get Supplier Name
                sql = "Select Distinct Supplier_Name From Supplier Where Supplier_ID = '" + Supplier + "'";
                sdr = pro.ReturnMDetails(sql);
                if (sdr.Read() == true)
                {
                    txtSup.Text = sdr["Supplier_Name"].ToString().Trim();
                }


                sql = "Select Count(1) From Book_Transaction Where Book_ID = '" + LstBook.SelectedItem.Text + "' And Actual_Return_Date Is Null";
                sdr = pro.ReturnMDetails(sql);
                if (sdr.Read() == true)
                {
                    AQuantity -= (int)sdr[0];
                }
            }
            sdr.Close();
            txtQuantity.Text = Convert.ToString(AQuantity);
            return;
        }
        catch (NullReferenceException nex)
        {
            lblEMsg.Visible = true;
            lblEMsg.Text = nex.Message;
            return;
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg.Visible = true;
            lblEMsg.Text = ex.Message;
        }

    }
    protected void btnCal_Click(object sender, EventArgs e)
    {
        Calendar1.Visible = true;
        Calendar1.Focus();
    }
    protected void Calendar1_SelectionChanged(object sender, EventArgs e)
    {
        lblEMsg.Visible = false;
        
        if (Convert.ToDateTime(txtIDate.Text) > Convert.ToDateTime(Calendar1.SelectedDate.ToString()))
        {
            lblEMsg.Visible = true;
            lblEMsg.Text = "Return Date Has Already Past!.";
            return;
        }
        txtRDate.Text = Calendar1.SelectedDate.ToShortDateString();
        Calendar1.Visible = false;
        btnIssue.Focus();
    }
    protected void btnIssue_Click(object sender, EventArgs e)
    {
        lblEMsg.Visible = false;
        string[] iBook;
        try
        {
            SqlDataReader sdr;
            Profile pro = new Profile();

            int aQty = Convert.ToInt32(txtQuantity.Text.ToString());
            if (aQty == 0)
            {
                lblEMsg.Visible = true;
                lblEMsg.Text = "This Book is Out of Stock, Please Select a Different Book!";
                return;
            }
            
            string sql = "SELECT Count(1) FROM Book_Transaction WHERE Book_ID = '" + LstBook.SelectedItem.Text + "' And Member_ID = '" + DropDownListRegistration.SelectedItem.Text  + "' And Actual_Return_Date Is Null";
            sdr = pro.ReturnMDetails(sql);
            sdr.Read();
            int iB = (int)sdr[0];
            if (iB > 0)
            {
                lblEMsg.Visible = true;
                lblEMsg.Text = "This Book is Already Issued, Please Select a Different Book!";
                return;
            }
            sdr.Close();

            //Insert the data.
            iBook = new string[4];

            iBook[0] = LstBook.SelectedItem.Text;
            iBook[1] = DropDownListRegistration.SelectedItem.Text;
            //iBook[2] = txtIDate.Text;
            //iBook[3] = txtRDate.Text;
            iBook[2] = DateTime.Now.ToString();
            iBook[3] = DateTime.Now.ToString();
            
            bool status;
            status = pro.IssueBook(iBook);

            if (status == false)
            {
                lblEMsg.Visible = true;
                lblEMsg.Text = "Book Issuing Failed";
            }
            lblEMsg.Visible = true;
            lblEMsg.Text = "Book Issued.";
            DropDownListRegistration.Text = "";
            LstBook.Text = "";
            LstBTitle.Text = "";
            txtAuthor.Text = "";
            txtPub.Text = "";
            txtCat.Text = "";
            txtSup.Text = "";
            txtQuantity.Text = "";
            txtPrice.Text = "";
            txtRDate.Text = "";
            lblFullName.Text = "";
            
            //Response.Redirect("IssueBooks.aspx",false);
            return;
        }
        catch (Exception ex)
        {
            lblEMsg.Visible = true;
            lblEMsg.Text = ex.Message;
        }

    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Response.Redirect("IssueBooks.aspx",false);
    }
}
