﻿using System;
using System.Configuration;
using System.Data;
//using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {

        // Put user code to initialize the page here
        Session["MEMBER_ID"] = "";
        Session["USER_NAME"] = "";
        Session["PASSWORD"] = "";
        Session["NAME"] = "";
        Session["STATUS"] = "";
        Session["USER_TYPE"] = "";
        Session.Abandon();
        FormsAuthentication.SignOut();
        
        Session.Clear();
        Session.Contents.RemoveAll();
        Session.Abandon();
        Response.Redirect("Login.aspx",true);
        
    }
}
