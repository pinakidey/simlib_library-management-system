﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

public partial class Register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        lblErrorMsg.Visible = false;
        if (Page.IsPostBack == false)
        {
            try
            {
                string sql = "SELECT COUNT(*) FROM MEMBER";
                SqlDataReader sdr;
                Profile pro = new Profile();
                sdr = pro.ReturnMDetails(sql);

                sdr.Read();
                int RegNo = (int)sdr[0] + 1;
                if (RegNo < 10)
                {
                    txtReg.Text = string.Format("M000{0}", RegNo);
                }
                else if (RegNo > 10 && RegNo < 100)
                {
                    txtReg.Text = string.Format("M00{0}", RegNo);
                }
                else
                {
                    txtReg.Text = string.Format("M0{0}", RegNo);
                }
                sdr.Close();
                txtUserName.Focus();
                return;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        
    }
    protected void LoginButton_Click(object sender, EventArgs e)
    {
        try
        {
            string strRole;

            string sql = "SELECT MEMBER_ID, USER_NAME, PASSWORD, FIRST_NAME, MIDDLE_NAME, LAST_NAME, STATUS, USER_TYPE FROM MEMBER WHERE USER_NAME = '" + UserName.Text + "' AND PASSWORD = '" + Password.Text + "'";
            SqlDataReader sdr;

            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);
            if (sdr.Read() == true)
            {
                FormsAuthentication.Initialize();
                if (sdr["USER_TYPE"].ToString().Trim() == "A")
                {
                    strRole = "Admin";
                }
                else
                {
                    strRole = "Member";
                }

                //The AddMinutes determines how long the user will be logged in after leaving
                //the site if he doesn't log off.
                //FormsAuthenticationTicket fat = new FormsAuthenticationTicket(1, txtUserName.Text, DateTime.Now, 
                //        DateTime.Now.AddMinutes(30), false, strRole, FormsAuthentication.FormsCookiePath);

                //Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, 
                //       FormsAuthentication.Encrypt(fat)));



                Session["MEMBER_ID"] = sdr["MEMBER_ID"].ToString().Trim();
                Session["USER_NAME"] = sdr["USER_NAME"].ToString().Trim();
                Session["PASSWORD"] = sdr["PASSWORD"].ToString().Trim();
                Session["NAME"] = sdr["FIRST_NAME"].ToString().Trim() + " " + sdr["MIDDLE_NAME"].ToString().Trim() + " " + sdr["LAST_NAME"].ToString().Trim();
                Session["STATUS"] = sdr["STATUS"].ToString().Trim();
                Session["USER_TYPE"] = sdr["USER_TYPE"].ToString().Trim();
                //Response.Redirect(FormsAuthentication.GetRedirectUrl("Home.aspx", false));
                Response.Redirect("Home.aspx",true);
            }
            else
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Invalid User Name/Password.";
            }
            sdr.Close();
        }
        catch (Exception err)
        {
            lblErrorMsg.Visible = true;
            lblErrorMsg.Text = err.Message;
        }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        txtUserName.Text = "";
        txtPassword.Text = "";
        ConfirmPassword.Text = "";
        txtFName.Text = "";
        txtMName.Text = "";
        txtLName.Text = "";
        txtAddress.Text = "";
        Email.Text = "";
        txtLline.Text = "";
        txtMobile.Text = "";
        optMale.Checked = true;
        optFemale.Checked = false;
        Question.Text = "";
        Answer.Text = "";
    }
    protected void btnSignUp_Click(object sender, EventArgs e)
    {
        try
        {
            string[] MProfile;
            lblEMsg.Visible = false;

            string sql = "SELECT COUNT(*) FROM MEMBER WHERE USER_NAME = '" + txtUserName.Text + "'";
            SqlDataReader sdr;
            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);

            sdr.Read();
            int RegNo = (int)sdr[0];
            if (RegNo > 0)
            {
                lblEMsg.Text = "User Account Already Exists!";
                lblEMsg.Visible = true;
                sdr.Close();
                return;
            }
            sdr.Close();

            sql = "SELECT COUNT(*) FROM MEMBER";
            sdr = pro.ReturnMDetails(sql);
            sdr.Read();
            RegNo = (int)sdr[0] + 1;
            if (RegNo < 10)
            {
                txtReg.Text = string.Format("M000{0}", RegNo);
            }
            else if (RegNo > 10 && RegNo < 100)
            {
                txtReg.Text = string.Format("M00{0}", RegNo);
            }
            else
            {
                txtReg.Text = string.Format("M0{0}", RegNo);
            }
            sdr.Close();

            MProfile = new string[15];

            MProfile[0] = txtReg.Text;
            MProfile[1] = txtUserName.Text;
            MProfile[2] = txtPassword.Text;
            MProfile[3] = txtFName.Text;
            MProfile[4] = txtMName.Text;
            MProfile[5] = txtLName.Text;
            MProfile[6] = txtAddress.Text;
            MProfile[7] = Email.Text;
            MProfile[8] = txtLline.Text;
            MProfile[9] = txtMobile.Text;
            if (optMale.Checked == true)
            {
                MProfile[10] = "M";
            }
            else
            {
                MProfile[10] = "F";
            }
            MProfile[11] = Question.Text;
            MProfile[12] = Answer.Text;
            MProfile[13] = "A";
            MProfile[14] = "M";

            bool status;
            status = pro.AddProfile(MProfile);

            if (status == false)
            {
                lblEMsg.Visible = true;
                lblEMsg.Text = "User Creation Failed";
            }
            else
            {
                Session["MEMBER_ID"] = MProfile[0];
                Session["USER_NAME"] = MProfile[1];
                Session["PASSWORD"] = MProfile[2];
                Session["NAME"] = MProfile[3] + " " + MProfile[4] + " " + MProfile[5];
                Session["STATUS"] = MProfile[13];
                Session["USER_TYPE"] = MProfile[14];

                lblEMsg.Text = "Your Account Has Been Successfully Created! Please Login!";
                
                Response.Redirect("Login.aspx",true);
                return;
            }
        }
        catch (Exception ex)
        {
            lblEMsg.Visible = true;
            lblEMsg.Text = ex.Message;
        }
    }

    
}
