﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

public partial class ManageCategory : System.Web.UI.Page
{
    bool status = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["USER_TYPE"] == null)
        {
            Response.Redirect("Login.aspx", true);
        }
        if (Page.IsPostBack == false)
        {
            
            if (Session["USER_TYPE"].ToString() == "A")
            {
                HCheckedInBooks.Visible = false;
                lblName.Text = (string)Session["USER_NAME"].ToString();
            }
            else
            {
                Response.Redirect("Login.aspx", true);
                //Panel2.Visible = false;
                //lblName.Text = (string)Session["NAME"].ToString();
            }
            ReadCategory();
        }
        

    }
    private void ReadCategory()
    {
        try
        {
            string sql = "SELECT COUNT(*) FROM CATEGORY";
            SqlDataReader sdr;
            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);

            sdr.Read();
            int Cate = (int)sdr[0] + 1;
            if (Cate < 10)
            {
                txtCategory.Text = string.Format("C000{0}", Cate);
            }
            else if (Cate > 10 && Cate < 100)
            {
                txtCategory.Text = string.Format("C00{0}", Cate);
            }
            else
            {
                txtCategory.Text = string.Format("C0{0}", Cate);
            }
            sdr.Close();
            
            try
            {
                //Load all the Category First.
                string tsql = "SELECT Category_ID FROM CATEGORY WHERE STATUS = 'A' OR STATUS = 'I'";
                SqlDataReader tsdr;
                Profile tpro = new Profile();
                tsdr = pro.ReturnMDetails(tsql);
                lstid.Items.Clear();
                lstid.Items.Add("");
                while (tsdr.Read())
                {
                    lstid.Items.Add(tsdr["Category_ID"].ToString().Trim());
                }
                tsdr.Close();
                
            }
            catch (Exception ex)
            {
                //throw ex;
                lblEMsg1.Visible = true;
                lblEMsg1.Text = ex.Message;
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg.Visible = true;
            lblEMsg.Text = ex.Message;
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        lblEMsg.Visible = false;
        string[] Catg;
        try
        {
            string sql = "SELECT COUNT(*) FROM CATEGORY WHERE CATEGORY_NAME = '" + txtCName.Text + "' And Status = 'A'";
            SqlDataReader sdr;
            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);

            sdr.Read();
            int RegNo = (int)sdr[0];
            if (RegNo > 0)
            {
                lblEMsg.Text = "Category Name already exists.";
                lblEMsg.Visible = true;
                sdr.Close();
                return;
            }
            else
            {
                Catg = new string[15];

                Catg[0] = txtCategory.Text;
                Catg[1] = txtCName.Text;
                Catg[2] = txtcDesc.Text;
                if (chkStatus.Checked == true)
                {
                    Catg[3] = "A";
                }
                else
                {
                    Catg[3] = "I";
                }

                bool status;
                status = pro.AddCategory(Catg);

                if (status == false)
                {
                    lblEMsg.Visible = true;
                    lblEMsg.Text = "Category Creation Failed";
                }
                lblEMsg.Visible = true;
                lblEMsg.Text = "Category Added.";
                
                txtCName.Text = "";
                txtcDesc.Text = "";
                ReadCategory();
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg.Visible = true;
            lblEMsg.Text = ex.Message;
        }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        txtCName.Text = "";
        txtcDesc.Text = "";
        chkStatus.Checked = false;
        lblEMsg.Visible = false;

        txtCName1.Text = "";
        txtcDesc1.Text = "";
        chkStatus1.Checked = false;
        lblEMsg1.Visible = false;
    }
    protected void lstid_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            status = true;
            if (lstid.SelectedItem.ToString() == "")
            {
                lblEMsg1.Visible = true;
                lblEMsg1.Text = "Please Select the Category";
                return;
            }
            string sql = "SELECT Category_Name, Category_Description, Status FROM CATEGORY WHERE Category_ID = '" + lstid.SelectedItem.ToString() + "'";
            SqlDataReader sdr;
            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);

            if (sdr.Read() == true)
            {
                txtCName1.Text = sdr["Category_Name"].ToString().Trim();
                txtcDesc1.Text = sdr["Category_Description"].ToString().Trim();
                chkStatus1.Checked = true;
            }
            sdr.Close();
            return;
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg1.Visible = true;
            lblEMsg1.Text = ex.Message;
        }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        lblEMsg.Visible = false;
        string[] Catg;
        Profile pro = new Profile();
        try
        {
            if (lstid.Text == "")
            {
                lblEMsg1.Text = "Please Select Category ID";
                lblEMsg1.Visible = true;
            }
            else
            {
                Catg = new string[15];

                Catg[0] = lstid.Text;
                Catg[1] = txtCName1.Text;
                Catg[2] = txtcDesc1.Text;
                if (chkStatus1.Checked == true)
                {
                    Catg[3] = "A";
                }
                else
                {
                    Catg[3] = "I";
                }

                bool status;
                status = pro.UpdateCategory(Catg);

                if (status == false)
                {
                    lblEMsg1.Visible = true;
                    lblEMsg1.Text = "Category Updation Failed";
                }
                lblEMsg1.Visible = true;
                lblEMsg1.Text = "Category Updated.";

                txtCName1.Text = "";
                txtcDesc1.Text = "";
                ReadCategory();
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg1.Visible = true;
            lblEMsg1.Text = ex.Message;
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        lblEMsg1.Visible = false;
        Profile pro = new Profile();

        try
        {
            if (lstid.Text == "")
            {
                lblEMsg1.Text = "Please Select Category ID";
                lblEMsg1.Visible = true;
            }
            else
            {
                bool status;
                status = pro.DeleteCategory(lstid.SelectedItem.ToString());

                if (status == false)
                {
                    lblEMsg1.Visible = true;
                    lblEMsg1.Text = "Category Deletion Failed";
                }
                lblEMsg1.Visible = true;
                lblEMsg1.Text = "Category Deleted.";

                lstid.Items.Clear();

                txtCategory.Text = "";
                txtCName1.Text = "";
                txtcDesc1.Text = "";
                ReadCategory();
                
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg1.Visible = true;
            lblEMsg1.Text = ex.Message;
        }
    }
}
