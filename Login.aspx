﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
  Response.Buffer = true;  
  Response.Expires = 0;
  Response.CacheControl = "no-cache";
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>SimLib --- Login</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="Expires" content="0"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Pragma" content="no-cache"/>
<link href="default.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            font-family: Script;
        }
        .style2
        {
            font-family: "Brush Script MT";
            font-weight: bold;
            font-size: xx-large;
            text-align: right;
            color: #66CCFF;
        }
        .style3
        {
            font-family: "Times New Roman", Times, serif;
            font-size: medium;
        }
        .style4
        {
            font-family: "Times New Roman", Times, serif;
            font-size: medium;
            font-weight: normal;
            color: #000000;
        }
    </style>
</head>

<body>
	<div id="menu">
		<ul>
			<li class="active"><a href="#" title="">Home</a></li>
			<li><a href="#" title="">Search Books</a></li>
			<li><a href="#" title="">Support</a></li>
			<li><a href="#" title="">About Us</a></li>
		</ul>
	</div>
	<div id="logo">
		<%--<h1><a href="#"><font size= "24">S</font>im<font size= "24">L</font>ib ---</a><span 
                class="style1"><a href="#"><font size= "6">Library Management System</font></a></span></h1>--%>
        <asp:Image ID="Header" runat="server" AlternateText="SimLib" Height="55px" 
            ImageAlign="Middle" ImageUrl="~/images/simlib (740 x 90).png" 
            BorderStyle="Groove" Width="733px" />        
	</div>
<div id="content">
	<div id="sidebar">
		<div id="login" class="boxed">
			<h2 class="title">Sign In</h2>
			<div class="content">
				<form id="form1" runat="server">
					<fieldset>
					<legend>Sign-In</legend>
					
					<label for="inputtext1">User Name (Case Sensitive):</label> 
                    <asp:TextBox ID="UserName" runat="server" Height="23px" Width="150px" TabIndex="1" 
                            ToolTip="Enter UserName" ValidationGroup="Login1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ErrorMessage="UserName Cannot be Blank!" ControlToValidate="UserName" 
                            SetFocusOnError="True" ValidationGroup="Login1"></asp:RequiredFieldValidator>
                    <p>
                    </p>
					
					<label for="inputtext2">Password (Case Sensitive):</label>
					
					<p>
					
                    <asp:TextBox ID="Password" runat="server" Height="23px" Width="150px" TabIndex="2" 
                            TextMode="Password" ToolTip="Enter Password" ValidationGroup="Login1"></asp:TextBox>
					<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                            ErrorMessage="Password Cannot be Blank!" ControlToValidate="Password" 
                            SetFocusOnError="True" ValidationGroup="Login1"></asp:RequiredFieldValidator>
					
					</p>
					<p></p>
                    <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                        <asp:Label ID="lblErrorMsg" runat="server" Visible="False"></asp:Label>
                    <p></p>
                    <asp:Button ID="LoginButton" runat="server" Text="Sign In" TabIndex="3" 
                            ToolTip="Sign In" onclick="LoginButton_Click" ValidationGroup="Login1" 
                            Width="75px"/>
					<p><a href="./Register.aspx" tabindex="4">New User? Sign Up...</a></p>
					<p><a href="./Forgot.aspx" tabindex="5">Forgot your password?</a></p>
					</fieldset>
				</form>
			</div>
		</div>
		<div id="updates" class="boxed">
			<h2 class="title">Recent Addition</h2>
			<div class="content">
				<ul>
					<li>
						<h3>July 27, 2010</h3>
						<p>Mastering Visual Basic by Evangalos Petroutsos&#8230;</p>
					</li>
					<li>
						<h3>July 25, 2010</h3>
						<p>Principles of Genetics by Snustad & Simmons&#8230;</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div id="main">
		<div id="welcome" class="post">
			<h2 class="title" 
                style="font-family: Algerian; font-size: x-large">Welcome to SimLib!</h2>
			<div class="story">
				<p class="style2">Library is a click away...</p>
				<span class="style3">The SimLib offers online access to books & study materials from the library and archive of Bengal Institute of Technology.</span>
               
			</div>
		</div>
		<div id="example" class="post">
			<%--<div class="story">
			</div>--%>
		</div>
	</div>
</div>
<div id="footer">
	<p id="legal">Copyright &copy; 2010. All Rights Reserved. <%--Designed by <a href="http://www.pinakidey.emurse.com/">
        PINAKI DEY</a>.--%></p>
	<p id="links"><a href="#">Privacy Policy</a> | <a href="license.txt">Terms of Use</a></p>
</div>
</body>
</html>

