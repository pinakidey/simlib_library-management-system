﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;


public partial class ManageBooks : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["USER_TYPE"] == null)
        {
            Response.Redirect("Login.aspx", true);
        }
        if (Page.IsPostBack == false)
        {
            ReadBook();
            
            if (Session["USER_TYPE"].ToString() == "A")
            {
                HCheckedInBooks.Visible = false;
                lblName.Text = (string)Session["USER_NAME"].ToString();
            }
            else
            {
                Response.Redirect("Login.aspx", true);
                //Panel2.Visible = false;
                //lblName.Text = (string)Session["NAME"].ToString();
            }
        }
    }
    private void ReadBook()
    {
        try
        {
            string sql = "SELECT COUNT(*) FROM Book_Master";
            SqlDataReader sdr;
            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);

            sdr.Read();
            int Book = (int)sdr[0] + 1;
            if (Book < 10)
            {
                txtBook.Text = string.Format("B000{0}", Book);
            }
            else if (Book >= 10 && Book < 100)
            {
                txtBook.Text = string.Format("B00{0}", Book);
            }
            else
            {
                txtBook.Text = string.Format("B0{0}", Book);
            }
            sdr.Close();

            

            //Retrieve Author Details
            sql = "SELECT Distinct Author_Name FROM Author Where Status = 'A'";
            sdr = pro.ReturnMDetails(sql);
            LstAuthor.Items.Clear();
            LstAuthor.Items.Add("");
            while (sdr.Read())
            {
                LstAuthor.Items.Add(sdr["Author_Name"].ToString().Trim());
            }
            sdr.Close();

            //Retrieve Publisher Details
            sql = "SELECT Distinct Publisher_Name FROM Publisher Where Status = 'A'";
            sdr = pro.ReturnMDetails(sql);
            LstPublisher.Items.Clear();
            LstPublisher.Items.Add("");
            while (sdr.Read())
            {
                LstPublisher.Items.Add(sdr["Publisher_Name"].ToString().Trim());
            }
            sdr.Close();

            //Retrieve Category Details
            sql = "SELECT Distinct Category_Name FROM Category Where Status = 'A'";
            sdr = pro.ReturnMDetails(sql);
            LstCategory.Items.Clear();
            LstCategory.Items.Add("");
            while (sdr.Read())
            {
                LstCategory.Items.Add(sdr["Category_Name"].ToString().Trim());
            }
            sdr.Close();

            //Retrieve Supplier Details
            sql = "SELECT Distinct Supplier_Name FROM Supplier Where Status = 'A'";
            sdr = pro.ReturnMDetails(sql);
            LstSupplier.Items.Clear();
            LstSupplier.Items.Add("");
            while (sdr.Read())
            {
                LstSupplier.Items.Add(sdr["Supplier_Name"].ToString().Trim());
            }
            sdr.Close();


            try
            {
                sql = "SELECT Distinct Book_ID FROM Book_Master Where Status = 'A'";
                sdr = pro.ReturnMDetails(sql);
                lstbook.Items.Clear();
                lstbook.Items.Add("");
                while (sdr.Read())
                {
                    lstbook.Items.Add(sdr["Book_ID"].ToString().Trim());
                }
                sdr.Close();

                //Retrieve Author Details
                sql = "SELECT Distinct Author_Name FROM Author Where Status = 'A'";
                sdr = pro.ReturnMDetails(sql);
                LstAuthor1.Items.Clear();
                LstAuthor1.Items.Add("");
                while (sdr.Read())
                {
                    LstAuthor1.Items.Add(sdr["Author_Name"].ToString().Trim());
                }
                sdr.Close();

                //Retrieve Publisher Details
                sql = "SELECT Distinct Publisher_Name FROM Publisher Where Status = 'A'";
                sdr = pro.ReturnMDetails(sql);
                LstPublisher1.Items.Clear();
                LstPublisher1.Items.Add("");
                while (sdr.Read())
                {
                    LstPublisher1.Items.Add(sdr["Publisher_Name"].ToString().Trim());
                }
                sdr.Close();

                //Retrieve Category Details
                sql = "SELECT Distinct Category_Name FROM Category Where Status = 'A'";
                sdr = pro.ReturnMDetails(sql);
                LstCategory1.Items.Clear();
                LstCategory1.Items.Add("");
                while (sdr.Read())
                {
                    LstCategory1.Items.Add(sdr["Category_Name"].ToString().Trim());
                }
                sdr.Close();

                //Retrieve Supplier Details
                sql = "SELECT Distinct Supplier_Name FROM Supplier Where Status = 'A'";
                sdr = pro.ReturnMDetails(sql);
                LstSupplier1.Items.Clear();
                LstSupplier1.Items.Add("");
                while (sdr.Read())
                {
                    LstSupplier1.Items.Add(sdr["Supplier_Name"].ToString().Trim());
                }
                sdr.Close();
            }
            catch (Exception ex)
            {
                //throw ex;
                lblEMsg1.Visible = true;
                lblEMsg1.Text = ex.Message;
            }

        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg.Visible = true;
            lblEMsg.Text = ex.Message;
        }


        
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        lblEMsg.Visible = false;
        string[] Book;
        string Author_ID, Publisher_ID, Category_ID, Supplier_ID;
        try
        {
            string sql;
            SqlDataReader sdr;
            Profile pro = new Profile();

            //Retrieve Author ID.
            sql = "SELECT Author_ID FROM Author WHERE Author_Name = '" + LstAuthor.SelectedItem.Text + "'";
            sdr = pro.ReturnMDetails(sql);
            if (sdr.Read() == true)
            {
                Author_ID = sdr["Author_ID"].ToString().Trim();
            }
            else
            {
                Author_ID = "";
            }
            sdr.Close();

            //Retrieve Publisher ID.
            sql = "SELECT Publisher_ID FROM Publisher WHERE Publisher_Name = '" + LstPublisher.SelectedItem.Text + "'";
            sdr = pro.ReturnMDetails(sql);
            if (sdr.Read() == true)
            {
                Publisher_ID = sdr["Publisher_ID"].ToString().Trim();
            }
            else
            {
                Publisher_ID = "";
            }
            sdr.Close();

            //Retrieve Category ID.
            sql = "SELECT Category_ID FROM Category WHERE Category_Name = '" + LstCategory.SelectedItem.Text + "'";
            sdr = pro.ReturnMDetails(sql);
            if (sdr.Read() == true)
            {
                Category_ID = sdr["Category_ID"].ToString().Trim();
            }
            else
            {
                Category_ID = "";
            }
            sdr.Close();

            //Retrieve Supplier ID.
            sql = "SELECT Supplier_ID FROM Supplier WHERE Supplier_Name = '" + LstSupplier.SelectedItem.Text + "'";
            sdr = pro.ReturnMDetails(sql);
            if (sdr.Read() == true)
            {
                Supplier_ID = sdr["Supplier_ID"].ToString().Trim();
            }
            else
            {
                Supplier_ID = "";
            }
            sdr.Close();

            sql = "SELECT COUNT(*) FROM Book_Master WHERE Book_Title = '" + txtBTitle.Text + "' And Author_ID ='" + Author_ID + "' And Publisher_ID = '" + Publisher_ID + "' And Status = 'A'";
            sdr = pro.ReturnMDetails(sql);

            sdr.Read();
            int RegNo = (int)sdr[0];
            if (RegNo > 0)
            {
                lblEMsg.Text = "Book Name Already Exists.";
                lblEMsg.Visible = true;
                sdr.Close();
                return;
            }
            else
            {
                Book = new string[15];

                Book[0] = txtBook.Text;
                Book[1] = txtBTitle.Text;
                Book[2] = Author_ID;
                Book[3] = Publisher_ID;
                Book[4] = Category_ID;
                Book[5] = Supplier_ID;
                Book[6] = txtKey.Text;
                Book[7] = txtIsbn.Text;
                Book[8] = txtQuantity.Text;
                Book[9] = txtPrice.Text;
                if (chkStatus.Checked == true)
                {
                    Book[10] = "A";
                }
                else
                {
                    Book[10] = "I";
                }

                bool status;
                status = pro.Books(Book, "AddBook");

                if (status == false)
                {
                    lblEMsg.Visible = true;
                    lblEMsg.Text = "Book creation Failed";
                }
                lblEMsg.Visible = true;
                lblEMsg.Text = "Book Added.";
                txtBTitle.Text = "";
                LstAuthor.Text = "";
                LstCategory.Text = "";
                LstPublisher.Text = "";
                LstSupplier.Text = "";
                txtKey.Text = "";
                txtIsbn.Text = "";
                txtQuantity.Text = "";
                txtPrice.Text = "";
                chkStatus.Checked = false;
                //lblEMsg.Visible = false;
                ReadBook();
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg.Visible = true;
            lblEMsg.Text = ex.Message;
        }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        txtBTitle.Text = "";
        LstAuthor.Text = "";
        LstCategory.Text = "";
        LstPublisher.Text = "";
        LstSupplier.Text = "";
        txtKey.Text = "";
        txtIsbn.Text = "";
        txtQuantity.Text = "";
        txtPrice.Text = "";
        chkStatus.Checked = false;
        lblEMsg.Visible = false;
    }
    protected void lstbook_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            bool status = true;
            string Author, Publisher, Category, Supplier;
            if (lstbook.SelectedItem.ToString() == "")
            {
                lblEMsg1.Visible = true;
                lblEMsg1.Text = "Please select the Book";
                return;
            }
            string sql = "SELECT Book_Title, Author_ID, Publisher_ID, Category_ID, Supplier_ID, Key_Word, ISBN, Quantity, Price, Status FROM Book_Master WHERE Book_ID = '" + lstbook.SelectedItem.ToString() + "'";
            SqlDataReader sdr;
            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);

            if (sdr.Read() == true)
            {
                txtBTitle1.Text = sdr["Book_Title"].ToString().Trim();

                Author = sdr["Author_ID"].ToString().Trim();
                Publisher = sdr["Publisher_ID"].ToString().Trim();
                Category = sdr["Category_ID"].ToString().Trim();
                Supplier = sdr["Supplier_ID"].ToString().Trim();

                txtKey1.Text = sdr["Key_Word"].ToString().Trim();
                txtIsbn1.Text = sdr["ISBN"].ToString().Trim();
                txtQuantity1.Text = sdr["Quantity"].ToString().Trim();
                txtPrice1.Text = sdr["Price"].ToString().Trim();
                chkStatus1.Checked = true;

                //Get Author Name.
                sql = "Select Distinct Author_Name From Author Where Author_ID = '" + Author + "'";
                sdr = pro.ReturnMDetails(sql);
                if (sdr.Read() == true)
                {
                    LstAuthor1.Text = sdr["Author_Name"].ToString().Trim();
                }
                //sdr.Close;

                //Get Publisher_Name
                sql = "Select Distinct Publisher_Name From Publisher Where Publisher_ID = '" + Publisher + "'";
                sdr = pro.ReturnMDetails(sql);
                if (sdr.Read() == true)
                {
                    LstPublisher1.Text = sdr["Publisher_Name"].ToString().Trim();
                }
                //sdr.Close;

                //Get Category Name
                sql = "Select Distinct Category_Name From Category Where Category_ID = '" + Category + "'";
                sdr = pro.ReturnMDetails(sql);
                if (sdr.Read() == true)
                {
                    LstCategory1.Text = sdr["Category_Name"].ToString().Trim();
                }
                //sdr.Close;

                //Get Supplier Name
                sql = "Select Distinct Supplier_Name From Supplier Where Supplier_ID = '" + Supplier + "'";
                sdr = pro.ReturnMDetails(sql);
                if (sdr.Read() == true)
                {
                    LstSupplier1.Text = sdr["Supplier_Name"].ToString().Trim();
                }
                //sdr.Close;
            }
            sdr.Close();
            return;
        }
        catch (NullReferenceException nex)
        {
            lblEMsg1.Visible = true;
            lblEMsg1.Text = "Please select the Book";
            return;
        }
        catch (Exception ex)
        {
             //throw ex;
            lblEMsg.Visible = true;
            lblEMsg.Text = ex.Message;
        }
    }
    private string ReturnData(string sql)
    {
        SqlDataReader sdr;
        Profile pro = new Profile();
        sdr = pro.ReturnMDetails(sql);
        if (sdr.Read() == true)
        {
            return sdr[0].ToString();
        }
        else
        {
            return "";
        }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        lblEMsg1.Visible = false;
        string[] Book;
        Profile pro = new Profile();
        string Author_ID, Publisher_ID, Category_ID, Supplier_ID;
        try
        {
            if (lstbook.SelectedItem.Text == "")
            {
                lblEMsg1.Text = "Please Select Book ID";
                lblEMsg1.Visible = true;
            }
            else
            {
                string sql;

                //Retrieve Author ID.
                sql = "SELECT Author_ID FROM Author WHERE Author_Name = '" + LstAuthor1.SelectedItem.Text + "'";
                Author_ID = ReturnData(sql);

                //Retrieve Publisher ID.
                sql = "SELECT Publisher_ID FROM Publisher WHERE Publisher_Name = '" + LstPublisher1.SelectedItem.Text + "'";
                Publisher_ID = ReturnData(sql);

                //Retrieve Category ID.
                sql = "SELECT Category_ID FROM Category WHERE Category_Name = '" + LstCategory1.SelectedItem.Text + "'";
                Category_ID = ReturnData(sql);

                //Retrieve Supplier ID.
                sql = "SELECT Supplier_ID FROM Supplier WHERE Supplier_Name = '" + LstSupplier1.SelectedItem.Text + "'";
                Supplier_ID = ReturnData(sql);

                Book = new string[15];

                Book[0] = lstbook.SelectedItem.Text;
                Book[1] = txtBTitle1.Text;
                Book[2] = Author_ID;
                Book[3] = Publisher_ID;
                Book[4] = Category_ID;
                Book[5] = Supplier_ID;
                Book[6] = txtKey1.Text;
                Book[7] = txtIsbn1.Text;
                Book[8] = txtQuantity1.Text;
                Book[9] = txtPrice1.Text;
                if (chkStatus1.Checked == true)
                {
                    Book[10] = "A";
                }
                else
                {
                    Book[10] = "D";
                }

                bool status;
                status = pro.Books(Book, "UpdateBook");

                if (status == false)
                {
                    lblEMsg1.Visible = true;
                    lblEMsg1.Text = "Book Updation Failed";
                }
                lblEMsg1.Visible = true;
                lblEMsg1.Text = "Book Updated.";

                lstbook.Text = "";
                txtBTitle1.Text = "";
                LstAuthor1.Text = "";
                LstCategory1.Text = "";
                LstPublisher1.Text = "";
                LstSupplier1.Text = "";
                txtKey1.Text = "";
                txtIsbn1.Text = "";
                txtQuantity1.Text = "";
                txtPrice1.Text = "";
                chkStatus1.Checked = false;
                //lblEMsg1.Visible = false;

                ReadBook();
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg1.Visible = true;
            lblEMsg1.Text = ex.Message;
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        lblEMsg1.Visible = false;
        Profile pro = new Profile();

        try
        {
            if (lstbook.SelectedItem.ToString() == "")
            {
                lblEMsg1.Text = "Please Select Book No.";
                lblEMsg1.Visible = true;
            }
            else
            {
                bool status;
                status = pro.DeleteBook(lstbook.SelectedItem.ToString());

                if (status == false)
                {
                    lblEMsg1.Visible = true;
                    lblEMsg1.Text = "Book Deletion Failed";
                }
                lblEMsg1.Visible = true;
                lblEMsg1.Text = "Book Deleted.";

                lstbook.Items.Clear();

                //Load all the Publisher First.
                string sql = "SELECT Book_ID FROM Book_Master where Status = 'A'";
                SqlDataReader sdr;
                sdr = pro.ReturnMDetails(sql);
                lstbook.Items.Add("");
                while (sdr.Read())
                {
                    lstbook.Items.Add(sdr["Book_ID"].ToString().Trim());
                }
                sdr.Close();
                txtBTitle1.Text = "";
                LstAuthor1.Text = "";
                LstCategory1.Text = "";
                LstPublisher1.Text = "";
                LstSupplier1.Text = "";
                txtKey1.Text = "";
                txtIsbn1.Text = "";
                txtQuantity1.Text = "";
                txtPrice1.Text = "";
                chkStatus1.Checked = false;
                //lblEMsg1.Visible = false;
                ReadBook();
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg1.Visible = true;
            lblEMsg1.Text = ex.Message;
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtBTitle1.Text = "";
        LstAuthor1.Text = "";
        LstCategory1.Text = "";
        LstPublisher1.Text = "";
        LstSupplier1.Text = "";
        txtKey1.Text = "";
        txtIsbn1.Text = "";
        txtQuantity1.Text = "";
        txtPrice1.Text = "";
        chkStatus1.Checked = false;
        lblEMsg1.Visible = false;
    }
}
