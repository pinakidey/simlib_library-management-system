﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class Home : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["USER_TYPE"] == null)
        {
            Response.Redirect("Login.aspx",true);
        }

        if (Session["USER_TYPE"].ToString() == "A")
        {
            HCheckedInBooks.Visible = false;
            
            lblName.Text = (string)Session["USER_NAME"].ToString();
        }
        else
        {
            
            Panel2.Visible = false;
            //HSMember.Visible = false;
            lblName.Text = (string)Session["NAME"].ToString();
        }

    }
}
