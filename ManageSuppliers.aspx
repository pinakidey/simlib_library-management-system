﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ManageSuppliers.aspx.cs" Inherits="ManageSuppliers" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
  Response.Buffer = true;  
  Response.Expires = 0;
  Response.CacheControl = "no-cache";
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>SimLib --- Manage Suppliers</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="Expires" content="0"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Pragma" content="no-cache"/>
<%--<link href="tablestyle.css" rel="stylesheet" type="text/css" />
--%>
<link href="ManageSuppliers.css" rel="stylesheet" type="text/css" />
<link href="Tab.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="Tabpane.js" type="text/javascript"> </script>

    <script type="text/javascript">
        window.onload = function () {
          
    };
    </script>
    
    <!-- tabpane.js -->
    <%--<script type="text/javascript">
        function hasSupport() {

          if (typeof hasSupport.support != "undefined")
            return hasSupport.support;
          
          var ie55 = /msie 5\.[56789]/i.test( navigator.userAgent );
          
          hasSupport.support = ( typeof document.implementation != "undefined" &&
              document.implementation.hasFeature( "html", "1.0" ) || ie55 )
              
          // IE55 has a serious DOM1 bug... Patch it!
          if ( ie55 ) {
            document._getElementsByTagName = document.getElementsByTagName;
            document.getElementsByTagName = function ( sTagName ) {
              if ( sTagName == "*" )
                return document.all;
              else
                return document._getElementsByTagName( sTagName );
            };
          }

          return hasSupport.support;
        }
        function WebFXTabPane( el, bUseCookie ) {
          if ( !hasSupport() || el == null ) return;
          
          this.element = el;
          this.element.tabPane = this;
          this.pages = [];
          this.selectedIndex = null;
          this.useCookie = bUseCookie != null ? bUseCookie : true;
          
          // add class name tag to class name
          this.element.className = this.classNameTag + " " + this.element.className;
          
          // add tab row
          this.tabRow = document.createElement( "div" );
          this.tabRow.className = "tab-row";
          el.insertBefore( this.tabRow, el.firstChild );

          var tabIndex = 0;
          if ( this.useCookie ) {
            tabIndex = Number( WebFXTabPane.getCookie( "webfxtab_" + this.element.id ) );
            if ( isNaN( tabIndex ) )
              tabIndex = 0;
          }
          this.selectedIndex = tabIndex;
          
          // loop through child nodes and add them
          var cs = el.childNodes;
          var n;
          for (var i = 0; i < cs.length; i++) {
            if (cs[i].nodeType == 1 && cs[i].className == "tab-page") {
              this.addTabPage( cs[i] );
            }
          }
        }

        WebFXTabPane.prototype.classNameTag = "dynamic-tab-pane-control";

        WebFXTabPane.prototype.setSelectedIndex = function ( n ) {
          if (this.selectedIndex != n) {
            if (this.selectedIndex != null && this.pages[ this.selectedIndex ] != null )
              this.pages[ this.selectedIndex ].hide();
            this.selectedIndex = n;
            this.pages[ this.selectedIndex ].show();
            
            if ( this.useCookie )
              WebFXTabPane.setCookie( "webfxtab_" + this.element.id, n );  // session cookie
          }
        };
          
        WebFXTabPane.prototype.getSelectedIndex = function () {
          return this.selectedIndex;
        };
          
        WebFXTabPane.prototype.addTabPage = function ( oElement ) {
          if ( !hasSupport() ) return;
          
          if ( oElement.tabPage == this )  // already added
            return oElement.tabPage;

          var n = this.pages.length;
          var tp = this.pages[n] = new WebFXTabPage( oElement, this, n );
          tp.tabPane = this;
          
          // move the tab out of the box
          this.tabRow.appendChild( tp.tab );
              
          if ( n == this.selectedIndex )
            tp.show();
          else
            tp.hide();
            
          return tp;
        };
          
        WebFXTabPane.prototype.dispose = function () {
          this.element.tabPane = null;
          this.element = null;    
          this.tabRow = null;
          
          for (var i = 0; i < this.pages.length; i++) {
            this.pages[i].dispose();
            this.pages[i] = null;
          }
          this.pages = null;
        };



        // Cookie handling
        WebFXTabPane.setCookie = function ( sName, sValue, nDays ) {
          var expires = "";
          if ( nDays ) {
            var d = new Date();
            d.setTime( d.getTime() + nDays * 24 * 60 * 60 * 1000 );
            expires = "; expires=" + d.toGMTString();
          }

          document.cookie = sName + "=" + sValue + expires + "; path=/";
        };

        WebFXTabPane.getCookie = function (sName) {
          var re = new RegExp( "(\;|^)[^;]*(" + sName + ")\=([^;]*)(;|$)" );
          var res = re.exec( document.cookie );
          return res != null ? res[3] : null;
        };

        WebFXTabPane.removeCookie = function ( name ) {
          setCookie( name, "", -1 );
        };
        function WebFXTabPage( el, tabPane, nIndex ) {
          if ( !hasSupport() || el == null ) return;
          
          this.element = el;
          this.element.tabPage = this;
          this.index = nIndex;
          
          var cs = el.childNodes;
          for (var i = 0; i < cs.length; i++) {
            if (cs[i].nodeType == 1 && cs[i].className == "tab") {
              this.tab = cs[i];
              break;
            }
          }
          
          // insert a tag around content to support keyboard navigation
          
          
          var a = document.createElement( "A" );
          this.aElement = a;
          a.href = "#";
          a.onclick = function () { return false; };
          while ( this.tab.hasChildNodes() )
            a.appendChild( this.tab.firstChild );
          this.tab.appendChild( a );

          
          // hook up events, using DOM0
          var oThis = this;
          this.tab.onclick = function () { oThis.select(); };
          this.tab.onmouseover = function () { WebFXTabPage.tabOver( oThis ); };
          this.tab.onmouseout = function () { WebFXTabPage.tabOut( oThis ); };
        }

        WebFXTabPage.prototype.show = function () {
          var el = this.tab;
          var s = el.className + " selected";
          s = s.replace(/ +/g, " ");
          el.className = s;
          
          this.element.style.display = "block";
        };

        WebFXTabPage.prototype.hide = function () {
          var el = this.tab;
          var s = el.className;
          s = s.replace(/ selected/g, "");
          el.className = s;

          this.element.style.display = "none";
        };
          
        WebFXTabPage.prototype.select = function () {
          this.tabPane.setSelectedIndex( this.index );
        };
          
        WebFXTabPage.prototype.dispose = function () {
          this.aElement.onclick = null;
          this.aElement = null;
          this.element.tabPage = null;
          this.tab.onclick = null;
          this.tab.onmouseover = null;
          this.tab.onmouseout = null;
          this.tab = null;
          this.tabPane = null;
          this.element = null;
        };

        WebFXTabPage.tabOver = function ( tabpage ) {
          var el = tabpage.tab;
          var s = el.className + " hover";
          s = s.replace(/ +/g, " ");
          el.className = s;
        };

        WebFXTabPage.tabOut = function ( tabpage ) {
          var el = tabpage.tab;
          var s = el.className;
          s = s.replace(/ hover/g, "");
          el.className = s;
        };


        // This function initializes all uninitialized tab panes and tab pages
        function setupAllTabs() {
          if ( !hasSupport() ) return;

          var all = document.getElementsByTagName( "*" );
          var l = all.length;
          var tabPaneRe = /tab\-pane/;
          var tabPageRe = /tab\-page/;
          var cn, el;
          var parentTabPane;
          
          for ( var i = 0; i < l; i++ ) {
            el = all[i]
            cn = el.className;

            // no className
            if ( cn == "" ) continue;
            
            // uninitiated tab pane
            if ( tabPaneRe.test( cn ) && !el.tabPane )
              new WebFXTabPane( el );
          
            // unitiated tab page wit a valid tab pane parent
            else if ( tabPageRe.test( cn ) && !el.tabPage &&
                  tabPaneRe.test( el.parentNode.className ) ) {
              el.parentNode.tabPane.addTabPage( el );      
            }
          }
        }

        function disposeAllTabs() {
          if ( !hasSupport() ) return;
          
          var all = document.getElementsByTagName( "*" );
          var l = all.length;
          var tabPaneRe = /tab\-pane/;
          var cn, el;
          var tabPanes = [];
          
          for ( var i = 0; i < l; i++ ) {
            el = all[i]
            cn = el.className;

            // no className
            if ( cn == "" ) continue;
            
            // tab pane
            if ( tabPaneRe.test( cn ) && el.tabPane )
              tabPanes[tabPanes.length] = el.tabPane;
          }
          
          for (i = tabPanes.length - 1; i >= 0; i--) {
            tabPanes[i].dispose();
            tabPanes[i] = null;
          }
        }


        // initialization hook up

        // DOM2
        if ( typeof window.addEventListener != "undefined" )
          window.addEventListener( "load", setupAllTabs, false );

        // IE 
        else if ( typeof window.attachEvent != "undefined" ) {
          window.attachEvent( "onload", setupAllTabs );
          window.attachEvent( "onunload", disposeAllTabs );
        }

        else {
          if ( window.onload != null ) {
            var oldOnload = window.onload;
            window.onload = function ( e ) {
              oldOnload( e );
              setupAllTabs();
            };
          }
          else 
            window.onload = setupAllTabs;
        }
        </script>--%>



    <style type="text/css">
        .style2
        {
            width: 149px;
        }
        .style3
        {
            width: 92px;
        }
        
       
        </style>
     
     <!-- tab.css -->
    <%--<style type="text/css">
    .dynamic-tab-pane-control.tab-pane {
      position:  relative;
      width:    100%;    /* width needed weird IE bug */
      margin-right:  -2px;  /* to make room for the shadow */
    }

    .dynamic-tab-pane-control .tab-row .tab {

      width:        70px;
      height:        16px;
      background-image:  url( "lunaImage/tab.png" );
      
      position:    relative;
      top:      0;
      display:    inline;
      float:      left;
      overflow:    hidden;
      
      cursor:      Default;

      margin:      1px -1px 1px 2px;
      padding:    2px 0px 0px 0px;
      border:      0;

      z-index:    1;
      font:      11px Tahoma;
      white-space:  nowrap;
      text-align:    center;
    }

    .dynamic-tab-pane-control .tab-row .tab.selected {
      width:        74px !important;
      height:        18px !important;
      background-image:  url( "lunaImage/tab.active.png" ) !important;
      background-repeat:  no-repeat;

      border-bottom-width:  0;
      z-index:    3;
      padding:    2px 0 0px 0;
      margin:      1px -3px -3px 0px;
      top:      -2px;
      font:        11px Tahoma;
    }

    .dynamic-tab-pane-control .tab-row .tab a {
      font:        11px Tahoma;
      color:        Black;
      text-decoration:  none;
      cursor:        default;
    }

    .dynamic-tab-pane-control .tab-row .tab.hover {
      font:        11px Tahoma;
      width:        70px;
      height:        16px;
      background-image:  url( "lunaImage/tab.hover.png" );
      background-repeat:  no-repeat;
    }


    .dynamic-tab-pane-control .tab-page {
      clear:      both;
      border:      1px solid rgb( 145, 155, 156 );
      background:    rgb( 252, 252, 254 );
      z-index:    2;
      position:    relative;
      top:      -2px;

      font:        11px Tahoma;
      color:        Black;

      /*filter:      progid:DXImageTransform.Microsoft.Gradient(StartColorStr=#fffcfcfe, EndColorStr=#fff4f3ee, GradientType=0)
              progid:DXImageTransform.Microsoft.Shadow(Color=#ff919899, Strength=2, Direction=135);*/
      
      /*244, 243, 238*/
      /* 145, 155, 156*/
      
      padding:    10px;
    }

    .dynamic-tab-pane-control .tab-row {
      z-index:    1;
      white-space:  nowrap;
    }
    </style>--%>

</head>

<body>
<form id="form3" runat="server">
	<div id="menu">
		<ul>
			<li class="active"><a href="Home.aspx" title="">Home</a></li>
			<li><a href="#" title="">Search Books</a></li>
			<li><a href="#" title="">Support</a></li>
			<li><a href="#" title="">About Us</a></li>
		</ul>
	</div>
	<div id="logo">
		<%--<h1><a href="#"><font size= "24">S</font>im<font size= "24">L</font>ib ---</a><span 
                class="style1"><a href="#"><font size= "6">Main Account</font></a></span>
        </h1>--%>
        <asp:Image ID="Header" runat="server" AlternateText="SimLib" Height="55px" 
            ImageAlign="Middle" ImageUrl="~/images/simlib (740 x 90).png" 
            BorderStyle="Groove" Width="733px" />
	</div>
<div id="content">
	<div id="sidebar">
		<div id="login" class="boxed">
			<h2 class="title" style="text-align: center">My Profile</h2>
			<div class="content">
				
					<fieldset>
                        <asp:Label ID="lblWelcome" runat="server" Text="Welcome" Font-Bold="True" 
                            Font-Names="Algerian" Font-Size="Large" ForeColor="#479AC6" Width="150px"></asp:Label>
					
                        <asp:Label ID="lblName" runat="server" Text="UserName" Font-Bold="False" 
                            Font-Names="Bernard MT Condensed" Font-Size="Small" ForeColor="#FFBD2E" 
                            Width="150px"></asp:Label>
                    <p>
                    </p>
					
                        <asp:HyperLink ID="HLogout" runat="server" Font-Bold="True" 
                            Font-Names="Bell MT" Font-Size="Small" ForeColor="Red" 
                            NavigateUrl="~/Default.aspx" Width="150px">Log Out</asp:HyperLink>
					
					<p>
					
                        &nbsp;</p>
					<p>
                        <asp:HyperLink ID="HEditProfile" runat="server" Font-Bold="True" 
                            Font-Names="Times New Roman" NavigateUrl="~/EditProfile.aspx" Target="_parent" 
                            ToolTip="Edit Your Profile" Width="150px">Edit Profile</asp:HyperLink>
                            </p>
                    
					
					</fieldset>
				
			</div>
		</div>
		<div id="updates" class="boxed">
			<h2 class="title" style="text-align: center">Utilities</h2>
			<div class="content">
				<ul>
					<li>
						<h3>
                            <asp:HyperLink ID="HSearch" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" NavigateUrl="~/Search.aspx" 
                                Width="150px" Target="_parent">Search Books</asp:HyperLink>
                        
                            <asp:HyperLink ID="HCheckedInBooks" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" NavigateUrl="~/CheckedIn.aspx" 
                                Target="_parent" Width="150px">CheckedIn Books</asp:HyperLink>
                        </h3>
						<p></p>
					</li>
                    <asp:Panel ID="Panel2" runat="server">
					<li>
						<h3 id="panel1">
                            <asp:HyperLink ID="HManBooks" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" NavigateUrl="~/ManageBooks.aspx" 
                                Target="_parent" Width="150px">Manage Books</asp:HyperLink>
                        
                            <asp:HyperLink ID="HManUsers" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" NavigateUrl="~/ManageUsers.aspx" 
                                Width="150px" Target="_parent">Manage Users</asp:HyperLink>    
                        
                            <asp:HyperLink ID="HManCat" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" 
                                NavigateUrl="~/ManageCategory.aspx" Target="_parent" Width="150px">Manage 
                            Category</asp:HyperLink>
                        
                            <asp:HyperLink ID="HManPub" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" 
                                NavigateUrl="~/ManagePublishers.aspx" Target="_parent" Width="150px">Manage 
                            Publishers</asp:HyperLink>
                            
                            <asp:HyperLink ID="HManAuthors" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" 
                                NavigateUrl="~/ManageAuthors.aspx" Target="_parent" Width="150px">Manage 
                            Authors</asp:HyperLink>
                        
                            <asp:HyperLink ID="HManSuppliers" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" 
                                NavigateUrl="~/ManageSuppliers.aspx" Target="_parent" Width="150px">Manage 
                            Suppliers</asp:HyperLink>
                        </h3>
						<p></p>
						<h3>
                            <asp:HyperLink ID="HIssue" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" NavigateUrl="~/IssueBooks.aspx" 
                                Target="_parent" Width="150px">Issue Books</asp:HyperLink>
						
                            <asp:HyperLink ID="HReturn" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" NavigateUrl="~/ReturnBooks.aspx" 
                                Target="_parent" Width="150px">Return Books</asp:HyperLink>
						</h3>
					</li>
					</asp:Panel>
				</ul>
			</div>
		</div>
	</div>
	<div id="main">
		<div id="welcome" class="post">
			<h2 class="title" 
                style="font-family: Algerian; font-size: x-large">Manage Suppliers
            </h2>
			<div class="story">
                <p></p>
                <div class="tab-pane" id="tabPane1">

                      <div class="tab-page" id="tabPage1">
                        <h4 class="tab">Add Supplier</h4>
                        <table><tr><td>
                          <table id="gradient-style" summary="Registration Details">
                    <tbody>
    	                <tr>
        	                <td class="style3">Supplier ID</td>
                            <td class="style2">
                                <asp:TextBox ID="txtSupplier" runat="server" ReadOnly="True" 
                                    ValidationGroup="Login1" Width="150px"></asp:TextBox></td>
                            <td></td>
                        </tr>
                        <tr>
        	                <td class="style3">Supplier Name</td>
                            <td class="style2">
                                <asp:TextBox ID="txtSName" runat="server" MaxLength="50"
                                    ToolTip="Enter Publisher Name" ValidationGroup="Login1" Width="150px"></asp:TextBox></td>
                            <td>
                                <asp:RequiredFieldValidator ID="PubNameRequired" runat="server" 
                                    ControlToValidate="txtSName" ToolTip="Supplier Name is Required." 
                                    ValidationGroup="Login1" Display="Dynamic" SetFocusOnError="True">Supplier 
                                Name is Required.</asp:RequiredFieldValidator></td>        	            
                        </tr>
                        
                        <tr>
        	                <td class="style3">Supplier Address</td>
                            <td class="style2">
                                <asp:TextBox ID="txtAdd" runat="server" TextMode="MultiLine" 
                                    ToolTip="Enter Publisher Address" ValidationGroup="Login1" 
                                    Width="150px"></asp:TextBox></td>
                            <td><asp:RequiredFieldValidator ID="CatDescRequired" runat="server" 
                                    ControlToValidate="txtAdd" ToolTip="Supplier Address is Required." 
                                    ValidationGroup="Login1" Display="Dynamic" SetFocusOnError="True">Supplier 
                                Address is Required.</asp:RequiredFieldValidator></td>
                        </tr>
                        
                        <tr>
        	                <td class="style3">Phone No.</td>
                            <td class="style2">
                                <asp:TextBox ID="txtPhone" runat="server" MaxLength="15"
                                    ToolTip="Enter Phone No." ValidationGroup="Login1" Width="150px"></asp:TextBox></td>
                            <td>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                    ControlToValidate="txtPhone" Display="Dynamic" SetFocusOnError="True" 
                                    ToolTip="Invalid Phone No." ValidationExpression="\d[0-9]*" 
                                    ValidationGroup="Login1">Invalid Phone No.</asp:RegularExpressionValidator>
                                <br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                    ControlToValidate="txtPhone" ToolTip="Phone No. is Required." 
                                    ValidationGroup="Login1" Display="Dynamic" SetFocusOnError="True">Phone No. 
                                is Required.</asp:RequiredFieldValidator></td>        	            
                        </tr>
                        
                        <tr>
        	                <td class="style3">FAX</td>
                            <td class="style2">
                                <asp:TextBox ID="txtFax" runat="server" MaxLength="15"
                                    ToolTip="Enter FAX No." ValidationGroup="Login1" Width="150px"></asp:TextBox></td>
                            <td><asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                    ControlToValidate="txtFax" Display="Dynamic" SetFocusOnError="True" 
                                    ToolTip="Invalid FAX No." ValidationExpression="\d[0-9]*" 
                                    ValidationGroup="Login1">Invalid FAX No.</asp:RegularExpressionValidator></td>        	            
                        </tr>
                        
                        <tr>
        	                <td class="style3">E-mail ID</td>
                            <td class="style2">
                                <asp:TextBox ID="txtemail" runat="server" MaxLength="50"
                                    ToolTip="Enter E-mail ID." ValidationGroup="Login1" Width="150px"></asp:TextBox></td>
                            <td><asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                    ControlToValidate="txtemail" Display="Dynamic" SetFocusOnError="True" 
                                    ToolTip="Invalid E-mail ID." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                    ValidationGroup="Login1">Invalid E-mail ID.</asp:RegularExpressionValidator></td>        	            
                        </tr>
                        
                        <tr>
        	                <td class="style3">Status</td>
                            <td class="style2">
                                <asp:CheckBox ID="chkStatus" runat="server" Checked="True" Text=" Available" 
                                    ValidationGroup="Login1" Width="150px" />
                                    </td>
                            <td></td>
                        </tr>
                        <tr>
        	                <td class="style3"></td>
                            <td class="style2" align="center">
                                <b></b>
                                <asp:Label
                                        ID="lblEMsg" runat="server" ForeColor="Red" Visible="False" 
                                    Font-Size="Larger"></asp:Label></td>
                            <td></td>
                        </tr>
                        <tr>
        	                <td class="style3" align="center">
                                </td>
                            <td class="style2" align="center">
                                <asp:Button ID="btnAdd" runat="server" Text="Add" ToolTip="Add Publisher" 
                                    ValidationGroup="Login1" onclick="btnAdd_Click" Width="75px"/></td>
                            <td align="center">
                                <asp:Button ID="btnReset" runat="server" Text="Reset"
                                    ToolTip="Reset" CausesValidation="False" onclick="btnReset_Click" /></td>
                        </tr>
                    </tbody>
                </table>
                        </td></tr></table>
                      </div>

                      <div class="tab-page" id="tabPage2">
                        <h4 class="tab">Edit Supplier</h4>
                        <table><tr><td>
                          <table id="Table1" summary="Registration Details">
                    <tbody>
    	                <tr>
        	                <td class="style3">Supplier ID</td>
                            <td class="style2">
                                <asp:DropDownList ID="lstid" runat="server" AutoPostBack="True" 
                                    CausesValidation="True" Width="150px" 
                                    onselectedindexchanged="lstid_SelectedIndexChanged">
                                </asp:DropDownList>
                                </td>
                            <td></td>
                        </tr>
                        <tr>
        	                <td class="style3">Supplier Name</td>
                            <td class="style2">
                                <asp:TextBox ID="txtSName1" runat="server" MaxLength="50"
                                    ToolTip="Enter Publisher Name" ValidationGroup="Login2" Width="150px"></asp:TextBox></td>
                            <td>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="txtSName1" ToolTip="Supplier Name is Required." 
                                    ValidationGroup="Login2" Display="Dynamic" SetFocusOnError="True">Supplier 
                                Name is Required.</asp:RequiredFieldValidator></td>        	            
                        </tr>
                        
                        <tr>
        	                <td class="style3">Supplier Address</td>
                            <td class="style2">
                                <asp:TextBox ID="txtAdd1" runat="server" TextMode="MultiLine" 
                                    ToolTip="Enter Publisher Address" ValidationGroup="Login2" 
                                    Width="150px"></asp:TextBox></td>
                            <td><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                    ControlToValidate="txtAdd1" ToolTip="Supplier Address is Required." 
                                    ValidationGroup="Login2" Display="Dynamic" SetFocusOnError="True">Supplier 
                                Address is Required.</asp:RequiredFieldValidator></td>
                        </tr>
                        
                        <tr>
        	                <td class="style3">Phone No.</td>
                            <td class="style2">
                                <asp:TextBox ID="txtPhone1" runat="server" MaxLength="15"
                                    ToolTip="Enter Phone No." ValidationGroup="Login2" Width="150px"></asp:TextBox></td>
                            <td>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                                    ControlToValidate="txtPhone1" Display="Dynamic" SetFocusOnError="True" 
                                    ToolTip="Invalid Phone No." ValidationExpression="\d[0-9]*" 
                                    ValidationGroup="Login2">Invalid Phone No.</asp:RegularExpressionValidator>
                                <br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                    ControlToValidate="txtPhone1" ToolTip="Phone No. is Required." 
                                    ValidationGroup="Login2" Display="Dynamic" SetFocusOnError="True">Phone No. 
                                is Required.</asp:RequiredFieldValidator></td>        	            
                        </tr>
                        
                        <tr>
        	                <td class="style3">FAX</td>
                            <td class="style2">
                                <asp:TextBox ID="txtFax1" runat="server" MaxLength="15"
                                    ToolTip="Enter FAX No." ValidationGroup="Login2" Width="150px"></asp:TextBox></td>
                            <td><asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" 
                                    ControlToValidate="txtFax1" Display="Dynamic" SetFocusOnError="True" 
                                    ToolTip="Invalid FAX No." ValidationExpression="\d[0-9]*" 
                                    ValidationGroup="Login2">Invalid FAX No.</asp:RegularExpressionValidator></td>        	            
                        </tr>
                        
                        <tr>
        	                <td class="style3">E-mail ID</td>
                            <td class="style2">
                                <asp:TextBox ID="txtemail1" runat="server" MaxLength="50"
                                    ToolTip="Enter E-mail ID" ValidationGroup="Login2" Width="150px"></asp:TextBox></td>
                            <td><asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" 
                                    ControlToValidate="txtemail1" Display="Dynamic" SetFocusOnError="True" 
                                    ToolTip="Invalid E-mail ID." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                    ValidationGroup="Login2">Invalid E-mail ID.</asp:RegularExpressionValidator></td>        	            
                        </tr>
                        
                        <tr>
        	                <td class="style3">Status</td>
                            <td class="style2">
                                <asp:CheckBox ID="chkStatus1" runat="server" Checked="True" Text=" Available" 
                                    ValidationGroup="Login1" Width="150px" />
                                    </td>
                            <td></td>
                        </tr>
                        <tr>
        	                <td class="style3"></td>
                            <td class="style2" align="center">
                                <b></b>
                                <asp:Label
                                        ID="lblEMsg1" runat="server" ForeColor="Red" Visible="False" 
                                    Font-Size="Larger"></asp:Label></td>
                            <td></td>
                        </tr>
                        <tr>
        	                <td class="style3" align="center">
        	                <asp:Button ID="btnUpdate" runat="server" Text="Update" ToolTip="Update Publisher" 
                                    ValidationGroup="Login2" Width="75px" onclick="btnUpdate_Click"/>
                                </td>
                            <td class="style2" align="center">
                                <asp:Button ID="btnDelete" runat="server" Text="Delete" ToolTip="Delete Publisher" 
                                    ValidationGroup="Login2" Width="75px" onclick="btnDelete_Click"/></td>
                            <td align="center">
                                <asp:Button ID="btnClear" runat="server" Text="Reset"
                                    ToolTip="Reset" CausesValidation="False" onclick="btnReset_Click" /></td>
                        </tr>
                    </tbody>
                </table>
                        </td></tr></table>
                      </div>
                  
                </div>

                <%--<asp:Image ID="imgHome" runat="server" AlternateText="Background image" 
                    ImageUrl="~/images/599px-wikipedia-logo_bw-hiressvg.png" Width="424px" 
                    Height="385px" />--%>
			</div>
		</div>
		<div id="example" class="post">
		</div>
	</div>
</div>

<div id="footer">
	<p id="legal">Copyright © 2010. All Rights Reserved. <%--Designed by <a href="http://www.pinakidey.emurse.com/">
        PINAKI DEY</a>.--%></p>
	<p id="links"><a href="#">Privacy Policy</a> | <a href="license.txt">Terms of Use</a></p>
</div>
</form>
</body>
</html>
