﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

public partial class Forgot : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void LoginButton_Click(object sender, EventArgs e)
    {
        try
        {
            string strRole;

            string sql = "SELECT MEMBER_ID, USER_NAME, PASSWORD, FIRST_NAME, MIDDLE_NAME, LAST_NAME, STATUS, USER_TYPE FROM MEMBER WHERE USER_NAME = '" + UserName.Text + "' AND PASSWORD = '" + Password.Text + "'";
            SqlDataReader sdr;

            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);
            if (sdr.Read() == true)
            {
                FormsAuthentication.Initialize();
                if (sdr["USER_TYPE"].ToString().Trim() == "A")
                {
                    strRole = "Admin";
                }
                else
                {
                    strRole = "Member";
                }

                //The AddMinutes determines how long the user will be logged in after leaving
                //the site if he doesn't log off.
                //FormsAuthenticationTicket fat = new FormsAuthenticationTicket(1, txtUserName.Text, DateTime.Now, 
                //        DateTime.Now.AddMinutes(30), false, strRole, FormsAuthentication.FormsCookiePath);

                //Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, 
                //       FormsAuthentication.Encrypt(fat)));



                Session["MEMBER_ID"] = sdr["MEMBER_ID"].ToString().Trim();
                Session["USER_NAME"] = sdr["USER_NAME"].ToString().Trim();
                Session["PASSWORD"] = sdr["PASSWORD"].ToString().Trim();
                Session["NAME"] = sdr["FIRST_NAME"].ToString().Trim() + " " + sdr["MIDDLE_NAME"].ToString().Trim() + " " + sdr["LAST_NAME"].ToString().Trim();
                Session["STATUS"] = sdr["STATUS"].ToString().Trim();
                Session["USER_TYPE"] = sdr["USER_TYPE"].ToString().Trim();
                //Response.Redirect(FormsAuthentication.GetRedirectUrl("Home.aspx", false));
                Response.Redirect("Home.aspx",true);
            }
            else
            {
                lblErrorMsg.Visible = true;
                lblErrorMsg.Text = "Invalid User Name/Password.";
            }
            sdr.Close();
        }
        catch (Exception err)
        {
            lblErrorMsg.Visible = true;
            lblErrorMsg.Text = err.Message;
        }
    }
    protected void btnFind_Click(object sender, EventArgs e)
    {
        lblEMsg.Visible = false;
        try
            {
                string sql = "SELECT Member_ID, Password, First_Name, E_Mail_ID, Security_Q, Security_A FROM Member WHERE (User_Name = '" + txtUserName.Text + "')";
                SqlDataReader sdr;
                Profile pro = new Profile();
                sdr = pro.ReturnMDetails(sql);
                sdr.Read();
                string UserName = sdr["Member_ID"].ToString().Trim();
                if (UserName == null)
                {
                    lblEMsg.Visible = true;
                    lblEMsg.Text = "User Name Does Not Exist!";
                    return;
                }
                else
                {
                    txtSecQue.Text = sdr["Security_Q"].ToString().Trim();
                    txtSecAns.Focus();
                }
                sdr.Close();
                return;
            }
            catch (Exception ex)
            {
                lblEMsg.Visible = true;
                lblEMsg.Text = "User Name Does Not Exist!";
                return;
            }
    }
    protected void btnRPass_Click(object sender, EventArgs e)
    {
        lblEMsg.Visible = false;
        try
        {
            string Member_ID, Password, First_Name, E_Mail_ID, Security_Q, Security_A;
            string sql = "SELECT Member_ID, Password, First_Name, E_Mail_ID, Security_Q, Security_A FROM Member WHERE (User_Name = '" + txtUserName.Text + "')";
            SqlDataReader sdr;
            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);
            sdr.Read();

            Member_ID = sdr["Member_ID"].ToString().Trim();
            Password = sdr["Password"].ToString().Trim();
            First_Name = sdr["First_Name"].ToString().Trim();
            E_Mail_ID = sdr["E_Mail_ID"].ToString().Trim();
            Security_Q = sdr["Security_Q"].ToString().Trim();
            Security_A = sdr["Security_A"].ToString().Trim();

            try
            {
                if (txtSecAns.Text == Security_A && txtFName.Text.ToLowerInvariant() == First_Name.ToLowerInvariant() && Email.Text==E_Mail_ID)
                {
                    lblEMsg.Visible = true;
                    lblEMsg.Text = "Password: " + Password;
                    
                }
                else
                {
                    lblEMsg.Visible = true;
                    lblEMsg.Text = "Credentials Are Incorrect!";
                    
                }

            }
            catch (Exception nex)
            {
                lblEMsg.Visible = true;
                lblEMsg.Text = nex.Message;
                
            }
        }
        catch (Exception ex)
        {
            lblEMsg.Visible = true;
            lblEMsg.Text = "User Name Does Not Exist!";
            
        }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        lblEMsg.Visible = false;
        txtSecAns.Text = "";
        txtFName.Text = "";
        Email.Text = "";
        txtSecAns.Focus();
    }
}
