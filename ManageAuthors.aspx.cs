﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

public partial class ManageAuthors : System.Web.UI.Page
{
    bool status = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["USER_TYPE"] == null)
        {
            Response.Redirect("Login.aspx", true);
        }
        if (Page.IsPostBack == false)
        {

            if (Session["USER_TYPE"].ToString() == "A")
            {
                HCheckedInBooks.Visible = false;
                lblName.Text = (string)Session["USER_NAME"].ToString();
            }
            else
            {
                Response.Redirect("Login.aspx", true);
                //Panel2.Visible = false;
                //lblName.Text = (string)Session["NAME"].ToString();
            }
            ReadAuthor();
        }

    }
    private void ReadAuthor()
    {
        try
        {
            string sql = "SELECT COUNT(*) FROM Author";
            SqlDataReader sdr;
            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);

            sdr.Read();
            int Auth = (int)sdr[0] + 1;
            if (Auth < 10)
            {
                txtAuthor.Text = string.Format("A000{0}", Auth);
            }
            else if (Auth >= 10 && Auth < 100)
            {
                txtAuthor.Text = string.Format("S00{0}", Auth);
            }
            else
            {
                txtAuthor.Text = string.Format("S0{0}", Auth);
            }
            sdr.Close();

            try
            {
                sql = "SELECT Author_ID FROM Author WHERE STATUS = 'A' OR STATUS = 'I'";
                sdr = pro.ReturnMDetails(sql);
                lstid.Items.Clear();
                lstid.Items.Add("");
                while (sdr.Read())
                {
                    lstid.Items.Add(sdr["Author_ID"].ToString().Trim());
                }
                sdr.Close();
            }
            catch (Exception nex)
            {
                //throw ex;
                lblEMsg1.Visible = true;
                lblEMsg1.Text = nex.Message;
            }

        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg.Visible = true;
            lblEMsg.Text = ex.Message;
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        lblEMsg.Visible = false;
        string[] Auth;
        try
        {
            string sql = "SELECT COUNT(*) FROM Author WHERE Author_NAME = '" + txtAName.Text + "' And Status = 'A'";
            SqlDataReader sdr;
            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);

            sdr.Read();
            int RegNo = (int)sdr[0];
            if (RegNo > 0)
            {
                lblEMsg.Text = "Author Name Already Exists.";
                lblEMsg.Visible = true;
                sdr.Close();
                return;
            }
            else
            {
                Auth = new string[15];

                Auth[0] = txtAuthor.Text;
                Auth[1] = txtAName.Text;
                //Auth[2] = txtAdd.Text;
                //Auth[3] = txtPhone.Text;
                //Auth[4] = txtFax.Text;
                Auth[5] = txtemail.Text;
                if (chkStatus.Checked == true)
                {
                    Auth[6] = "A";
                }
                else
                {
                    Auth[7] = "I";
                }

                bool status;
                status = pro.AddAuthor(Auth);

                if (status == false)
                {
                    lblEMsg.Visible = true;
                    lblEMsg.Text = "Author Creation Failed";
                }
                lblEMsg.Visible = true;
                lblEMsg.Text = "Author Added.";
                
                txtAName.Text = "";
                //txtAdd.Text = "";
                //txtPhone.Text = "";
                //txtFax.Text = "";
                txtemail.Text = "";

                ReadAuthor();
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg.Visible = true;
            lblEMsg.Text = ex.Message;
        }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        txtAName.Text = "";
        //txtAdd.Text = "";
        //txtPhone.Text = "";
        //txtFax.Text = "";
        txtemail.Text = "";
        chkStatus.Checked = false;
        lblEMsg.Visible = false;

        txtAName1.Text = "";
        //txtAdd.Text = "";
        //txtPhone.Text = "";
        //txtFax.Text = "";
        txtemail1.Text = "";
        chkStatus1.Checked = false;
        lblEMsg1.Visible = false;
    }
    protected void lstid_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            status = true;
            if (lstid.SelectedItem.ToString() == "")
            {
                lblEMsg1.Visible = true;
                lblEMsg1.Text = "Please Select the Author";
                return;
            }
            string sql = "SELECT Author_Name, EMail, Status FROM Author WHERE Author_ID = '" + lstid.SelectedItem.ToString() + "'";
            SqlDataReader sdr;
            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);

            if (sdr.Read() == true)
            {
                txtAName1.Text = sdr["Author_Name"].ToString().Trim();
                //txtAdd.Text = sdr["Address"].ToString().Trim();
                //txtPhone.Text = sdr["Phone"].ToString().Trim();
                //txtFax.Text = sdr["Fax"].ToString().Trim();
                txtemail1.Text = sdr["EMail"].ToString().Trim();
                chkStatus1.Checked = true;
            }
            sdr.Close();
            
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg1.Visible = true;
            lblEMsg1.Text = ex.Message;
        }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        lblEMsg1.Visible = false;
        string[] Auth;
        Profile pro = new Profile();
        try
        {
            if (lstid.SelectedItem.Text == "")
            {
                lblEMsg1.Text = "Please Select Author ID";
                lblEMsg1.Visible = true;
            }
            else
            {
                Auth = new string[15];

                Auth[0] = lstid.Text;
                Auth[1] = txtAName1.Text;
                //Auth[2] = txtAdd.Text;
                //Auth[3] = txtPhone.Text;
                //Auth[4] = txtFax.Text;
                Auth[5] = txtemail1.Text;
                if (chkStatus1.Checked == true)
                {
                    Auth[6] = "A";
                }
                else
                {
                    Auth[6] = "I";
                }

                bool status;
                status = pro.UpdateAuthor(Auth);

                if (status == false)
                {
                    lblEMsg1.Visible = true;
                    lblEMsg1.Text = "Author Updation Failed";
                }
                lblEMsg1.Visible = true;
                lblEMsg1.Text = "Author Updated.";

                
                txtAName1.Text = "";
                //txtAdd.Text = "";
                //txtPhone.Text = "";
                //txtFax.Text = "";
                txtemail1.Text = "";
                chkStatus1.Checked = false;
                ReadAuthor();
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg1.Visible = true;
            lblEMsg1.Text = ex.Message;
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        lblEMsg.Visible = false;
        Profile pro = new Profile();

        try
        {
            if (lstid.SelectedItem.Text == "")
            {
                lblEMsg1.Text = "Please Select Author ID";
                lblEMsg1.Visible = true;
            }
            else
            {
                bool status;
                status = pro.DeleteAuthor(lstid.SelectedItem.Text);

                if (status == false)
                {
                    lblEMsg1.Visible = true;
                    lblEMsg1.Text = "Author Deletion Failed";
                }
                lblEMsg1.Visible = true;
                lblEMsg1.Text = "Author Deleted.";
                lstid.Items.Clear();

                
                txtAName1.Text = "";
                //txtAdd.Text = "";
                //txtPhone.Text = "";
                //txtFax.Text = "";
                txtemail1.Text = "";
                chkStatus1.Checked = false;
                ReadAuthor();

            }
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg1.Visible = true;
            lblEMsg1.Text = ex.Message;
        }
    }
}
