﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
  Response.Buffer = true;  
  Response.Expires = 0;
  Response.CacheControl = "no-cache";
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>SimLib --- Register</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="Expires" content="0"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Pragma" content="no-cache"/>
<link href="tablestyle.css" rel="stylesheet" type="text/css" />
<link href="default1.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            font-family: Script;
        }
        .style2
        {
            width: 149px;
        }
        .style3
        {
            width: 92px;
        }
        
       
        </style>
</head>

<body>
<form id="form2" runat="server">
	<div id="menu">
		<ul>
			<li class="active"><a href="./Login.aspx" title="">Home</a></li>
			<li><a href="#" title="">Search Books</a></li>
			<li><a href="#" title="">Support</a></li>
			<li><a href="#" title="">About Us</a></li>
		</ul>
	</div>
	<div id="logo">
		<%--<h1><a href="#"><font size= "24">S</font>im<font size= "24">L</font>ib ---</a><span 
                class="style1"><a href="#"><font size= "6">Library Management System</font></a></span></h1>--%>
	    <asp:Image ID="Header" runat="server" AlternateText="SimLib" Height="55px" 
            ImageAlign="Middle" ImageUrl="~/images/simlib (740 x 90).png" 
            BorderStyle="Groove" Width="733px" />
	</div>
<div id="content">
	<div id="sidebar">
		<div id="login" class="boxed">
			<h2 class="title">Sign In</h2>
			<div class="content">
				<%--<form id="form1" runat="server">--%>
					<fieldset>
					<legend>Sign-In</legend>
					
					<label for="inputtext1">User Name (Case Sensitive):</label> 
                    <asp:TextBox ID="UserName" runat="server" Height="23px" Width="150px" TabIndex="1" 
                            ToolTip="Enter UserName" ValidationGroup="Login2"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ErrorMessage="UserName Cannot be Blank!" ControlToValidate="UserName" 
                            SetFocusOnError="True" ValidationGroup="Login2"></asp:RequiredFieldValidator>
                    <p>
                    </p>
					
					<label for="inputtext2">Password (Case Sensitive):</label>
					
					<p>
					
                    <asp:TextBox ID="Password" runat="server" Height="23px" Width="150px" TabIndex="2" 
                            TextMode="Password" ToolTip="Enter Password" ValidationGroup="Login2"></asp:TextBox>
					<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                            ErrorMessage="Password Cannot be Blank!" ControlToValidate="Password" 
                            SetFocusOnError="True" ValidationGroup="Login2"></asp:RequiredFieldValidator>
					
					</p>
					<p></p>
                    <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                        <asp:Label ID="lblErrorMsg" runat="server" Visible="False"></asp:Label>
                    <p></p>
                    <asp:Button ID="LoginButton" runat="server" Text="Sign In" TabIndex="3" 
                            ToolTip="Sign In" onclick="LoginButton_Click" ValidationGroup="Login2" 
                            Width="75px"/>
					<p><a href="./Forgot.aspx" tabindex="5">Forgot your password?</a></p>
					</fieldset>
				<%--</form>--%>
			</div>
		</div>
		<div id="updates" class="boxed">
			<h2 class="title">Recent Addition</h2>
			<div class="content">
				<ul>
					<li>
						<h3>July 27, 2010</h3>
						<p>Mastering Visual Basic by Evangalos Petroutsos…</p>
					</li>
					<li>
						<h3>July 25, 2010</h3>
						<p>Principles of Genetics by Snustad &amp; Simmons…</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div id="main">
		<div id="welcome" class="post">
			<h2 class="title" 
                style="font-family: Algerian; font-size: x-large">Sign Up for Your New Account</h2>
			<div class="story">
				<table id="gradient-style" summary="Registration Details">
                    <tbody>
    	                <tr>
        	                <td class="style3">Registration No.</td>
                            <td class="style2">
                                <asp:TextBox ID="txtReg" runat="server" ReadOnly="True" 
                                    ValidationGroup="Login1" Width="150px" 
                                    ToolTip="Use it for future references"></asp:TextBox></td>
                            <td><font color="red">Remember It!</font></td>
                        </tr>
                        <tr>
        	                <td class="style3">User Name</td>
                            <td class="style2">
                                <asp:TextBox ID="txtUserName" runat="server" MaxLength="20"
                                    ToolTip="Enter Desired UserName" ValidationGroup="Login1" Width="150px"></asp:TextBox></td>
                            <td>
                                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" 
                                    ControlToValidate="txtUserName" ToolTip="User Name is required." 
                                    ValidationGroup="Login1" Display="Dynamic" SetFocusOnError="True">User Name is required.</asp:RequiredFieldValidator></td>        	            
                        </tr>
                        <tr>
        	                <td class="style3">Password</td>
                            <td class="style2">
                                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="150px" 
                                    MaxLength="20" ToolTip="Enter Desired Password" ValidationGroup="Login1"></asp:TextBox></td>
                            <td>
                                <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="txtPassword" 
                                    ToolTip="Password is required." ValidationGroup="Login1" Display="Dynamic" 
                                    SetFocusOnError="True">Password is 
                                required.</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
        	                <td class="style3">Confirm Password</td>
                            <td class="style2">
                                <asp:TextBox ID="ConfirmPassword" runat="server" MaxLength="20" 
                                    TextMode="Password" ToolTip="Confirm Password" ValidationGroup="Login1" 
                                    Width="150px"></asp:TextBox></td>
                            <td>
                                <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" 
                                    ControlToValidate="ConfirmPassword" ToolTip="Confirm Password is required." 
                                    ValidationGroup="Login1" Display="Dynamic" SetFocusOnError="True">Confirm Password is required.</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
        	                <td class="style3">First Name</td>
                            <td class="style2">
                                <asp:TextBox ID="txtFName" runat="server" MaxLength="30" 
                                    ValidationGroup="Login1" Width="150px" ToolTip="Enter Your First Name"></asp:TextBox></td>
                            <td>
                                <asp:RequiredFieldValidator ID="FirstNameRequired" runat="server" 
                                    ControlToValidate="txtFName" ToolTip="First Name is Required" 
                                    ValidationGroup="Login1" Display="Dynamic" SetFocusOnError="True">First Name is Required</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
        	                <td class="style3">Middle Name</td>
                            <td class="style2">
                                <asp:TextBox ID="txtMName" runat="server" MaxLength="30" 
                                    ToolTip="Enter Your Middle Name" ValidationGroup="Login1" Width="150px"></asp:TextBox></td>
                            <td></td>
                        </tr>
                        <tr>
        	                <td class="style3">Last Name</td>
                            <td class="style2">
                                <asp:TextBox ID="txtLName" runat="server" MaxLength="30" 
                                    ToolTip="Enter Your Last Name" ValidationGroup="Login1" Width="150px"></asp:TextBox></td>
                            <td></td>
                        </tr>
                        <tr>
        	                <td class="style3">Address</td>
                            <td class="style2">
                                <asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" 
                                    ToolTip="Enter Your Address" ValidationGroup="Login1" Width="150px"></asp:TextBox></td>
                            <td>
                                <asp:RequiredFieldValidator ID="AddressRequired" runat="server" 
                                    ControlToValidate="txtAddress" ToolTip="Address is Required." 
                                    ValidationGroup="Login1" Display="Dynamic" SetFocusOnError="True">Address is Required.</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
        	                <td class="style3">E-mail</td>
                            <td class="style2">
                                <asp:TextBox ID="Email" runat="server" MaxLength="30" 
                                    ToolTip="Enter Valid Email ID" ValidationGroup="Login1" Width="150px"></asp:TextBox>
                                    </td>
                            <td>
                                <asp:RequiredFieldValidator ID="EmailRequired" runat="server" 
                                    ControlToValidate="Email" Display="Dynamic" ToolTip="Email is Required." 
                                    ValidationGroup="Login1" SetFocusOnError="True">Email is Required.</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator
                                        ID="RegularExpressionValidator1" runat="server" 
                                    ControlToValidate="Email" Display="Dynamic" ToolTip="Invalid Email ID." 
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                    ValidationGroup="Login1" SetFocusOnError="True">Invalid Email ID.</asp:RegularExpressionValidator></td>
                        </tr>
                        <tr>
        	                <td class="style3">Phone No.</td>
                            <td class="style2">
                                <asp:TextBox ID="txtLline" runat="server" MaxLength="15" 
                                    ToolTip="Enter Your Phone No." ValidationGroup="1" Width="150"></asp:TextBox></td>
                            <td>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                    ControlToValidate="txtLline" Display="Dynamic" ToolTip="Invalid Phone No." 
                                    ValidationExpression="\d[0-9]*" ValidationGroup="Login1" 
                                    SetFocusOnError="True">Invalid Phone No.</asp:RegularExpressionValidator></td>
                        </tr>
                        <tr>
        	                <td class="style3">Mobile No.</td>
                            <td class="style2">
                                <asp:TextBox ID="txtMobile" runat="server" MaxLength="15" 
                                    ToolTip="Enter Your Mobile No." ValidationGroup="Login1" Width="150px"></asp:TextBox></td>
                            <td>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                                    ControlToValidate="txtMobile" Display="Dynamic" ToolTip="Invalid Mobile No." 
                                    ValidationExpression="\d[0-9]*" ValidationGroup="Login1" 
                                    SetFocusOnError="True">Invalid Mobile No.</asp:RegularExpressionValidator></td>
                        </tr>
                        <tr>
        	                <td class="style3">Sex</td>
                            <td class="style2">
                                <asp:RadioButton ID="optMale" runat="server" Checked="True" GroupName="t1" 
                                    Text="Male" ValidationGroup="Login1" Width="65px" /><asp:RadioButton ID="optFemale"
                                    runat="server" GroupName="t1" Text="Female" ValidationGroup="Login1" 
                                    Width="75px" />
                                    </td>
                            <td></td>
                        </tr>
                        <tr>
        	                <td class="style3">Security Question</td>
                            <td class="style2">
                                <asp:TextBox ID="Question" runat="server" MaxLength="100" 
                                    ValidationGroup="Login1" Width="150px" ToolTip="Enter Security Question"></asp:TextBox></td>
                            <td>
                                <asp:RequiredFieldValidator ID="SecurityQuestionRequired" runat="server" 
                                    ControlToValidate="Question" Display="Dynamic" 
                                    ToolTip="Security Question is Required." ValidationGroup="Login1" 
                                    SetFocusOnError="True">Security 
                                Question is Required.</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
        	                <td class="style3">Security Answer</td>
                            <td class="style2">
                                <asp:TextBox ID="Answer" runat="server" MaxLength="100" TextMode="Password" 
                                    ToolTip="Enter Security Answer" ValidationGroup="Login1" Width="150px"></asp:TextBox></td>
                            <td>
                                <asp:RequiredFieldValidator ID="SecurityAnswerRequired" runat="server" 
                                    ControlToValidate="Answer" Display="Dynamic" 
                                    ToolTip="Security Answer is Required." ValidationGroup="Login1" 
                                    SetFocusOnError="True">Security 
                                Answer is Required.</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
        	                <td class="style3"></td>
                            <td class="style2" align="center">
                                <asp:CompareValidator ID="PasswordCompare" runat="server"
                                    ControlToCompare="txtPassword" ControlToValidate="ConfirmPassword" 
                                    Display="Dynamic" Font-Bold="False" Font-Italic="False" Font-Size="X-Small" 
                                    ToolTip="ReConfirm the Password Precisely" Width="150px" 
                                    ErrorMessage="Password and Confirmation Password Must Match." 
                                    ValidationGroup="Login1" SetFocusOnError="True"></asp:CompareValidator>
                                <b></b>
                                <asp:Label
                                        ID="lblEMsg" runat="server" ForeColor="Red" Visible="False" 
                                    Font-Size="Larger"></asp:Label></td>
                            <td></td>
                        </tr>
                        <tr>
        	                <td class="style3" align="center">
                                </td>
                            <td class="style2" align="center">
                                <asp:Button ID="btnSignUp" runat="server" Text="Sign Up" ToolTip="Sign Up" 
                                    ValidationGroup="Login1" onclick="btnSignUp_Click" /></td>
                            <td align="center">
                                <asp:Button ID="btnReset" runat="server" Text="Reset" onclick="btnReset_Click" 
                                    ToolTip="Reset" CausesValidation="False" /></td>
                        </tr>
                    </tbody>
                </table>
			</div>
		</div>
		<div id="example" class="post">
		</div>
	</div>
</div>

<div id="footer">
	<p id="legal">Copyright © 2010. All Rights Reserved. <%--Designed by <a href="http://www.pinakidey.emurse.com/">
        PINAKI DEY</a>.--%></p>
	<p id="links"><a href="#">Privacy Policy</a> | <a href="license.txt">Terms of Use</a></p>
</div>
</form>
</body>
</html>

