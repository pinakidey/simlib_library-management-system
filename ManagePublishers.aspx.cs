﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

public partial class ManagePublishers : System.Web.UI.Page
{
    bool status = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["USER_TYPE"] == null)
        {
            Response.Redirect("Login.aspx", true);
        }
        if (Page.IsPostBack == false)
        {

            if (Session["USER_TYPE"].ToString() == "A")
            {
                HCheckedInBooks.Visible = false;
                lblName.Text = (string)Session["USER_NAME"].ToString();
            }
            else
            {
                Response.Redirect("Login.aspx", true);
                //Panel2.Visible = false;
                //lblName.Text = (string)Session["NAME"].ToString();
            }
            ReadPublisher();
        }
    }
    private void ReadPublisher()
    {
        try
        {
            string sql = "SELECT COUNT(*) FROM Publisher";
            SqlDataReader sdr;
            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);

            sdr.Read();
            int Pub = (int)sdr[0] + 1;
            if (Pub < 10)
            {
                txtPublisher.Text = string.Format("P000{0}", Pub);
            }
            else if (Pub > 10 && Pub < 100)
            {
                txtPublisher.Text = string.Format("P00{0}", Pub);
            }
            else
            {
                txtPublisher.Text = string.Format("S0{0}", Pub);
            }
            sdr.Close();

            try
            {
                sql = "SELECT Publisher_ID FROM Publisher WHERE STATUS = 'A' OR STATUS = 'I'";
                sdr = pro.ReturnMDetails(sql);
                lstid.Items.Clear();
                lstid.Items.Add("");
                while (sdr.Read())
                {
                    lstid.Items.Add(sdr["Publisher_ID"].ToString().Trim());
                }
                sdr.Close();
            }
            catch (Exception nex)
            {
                //throw ex;
                lblEMsg1.Visible = true;
                lblEMsg1.Text = nex.Message;
            }

        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg.Visible = true;
            lblEMsg.Text = ex.Message;
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        lblEMsg.Visible = false;
        string[] Pub;
        try
        {
            string sql = "SELECT COUNT(*) FROM Publisher WHERE Publisher_NAME = '" + txtPName.Text + "' And Status = 'A'";
            SqlDataReader sdr;
            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);

            sdr.Read();
            int RegNo = (int)sdr[0];
            if (RegNo > 0)
            {
                lblEMsg.Text = "Publisher Name Already Exists.";
                lblEMsg.Visible = true;
                sdr.Close();
                return;
            }
            else
            {
                Pub = new string[15];

                Pub[0] = txtPublisher.Text;
                Pub[1] = txtPName.Text;
                //Pub[2] = txtPYear.Text;
                //Pub[3] = txtPHouse.Text;
                //Pub[4] = txtPEdition.Text;
                Pub[5] = txtAdd.Text;
                Pub[6] = txtPhone.Text;
                Pub[7] = txtFax.Text;
                Pub[8] = txtemail.Text;
                if (chkStatus.Checked == true)
                {
                    Pub[9] = "A";
                }
                else
                {
                    Pub[9] = "I";
                }

                bool status;
                status = pro.AddPublisher(Pub);

                if (status == false)
                {
                    lblEMsg.Visible = true;
                    lblEMsg.Text = "Publisher Creation Failed";
                }
                lblEMsg.Visible = true;
                lblEMsg.Text = "Publisher Added.";
                
                txtPName.Text = "";
                //txtPYear.Text = "";
                //txtPHouse.Text = "";
                //txtPEdition.Text = "";
                txtAdd.Text = "";
                txtPhone.Text = "";
                txtFax.Text = "";
                txtemail.Text = "";

                ReadPublisher();
            }
        }
        catch (Exception ex)
        {
            lblEMsg.Visible = true;
            lblEMsg.Text = ex.Message;
        }
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        txtPName.Text = "";
        //txtPYear.Text = "";
        //txtPHouse.Text = "";
        //txtPEdition.Text = "";
        txtAdd.Text = "";
        txtPhone.Text = "";
        txtFax.Text = "";
        txtemail.Text = "";
        chkStatus.Checked = false;
        lblEMsg.Visible = false;

        txtPName1.Text = "";
        //txtPYear.Text = "";
        //txtPHouse.Text = "";
        //txtPEdition.Text = "";
        txtAdd1.Text = "";
        txtPhone1.Text = "";
        txtFax1.Text = "";
        txtemail1.Text = "";
        chkStatus1.Checked = false;
        lblEMsg1.Visible = false;
    }
    protected void lstid_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            status = true;
            if (lstid.SelectedItem.ToString() == "")
            {
                lblEMsg1.Visible = true;
                lblEMsg1.Text = "Please Select the Publisher";
                return;
            }
            string sql = "SELECT Publisher_Name, Address, Phone, Fax, EMail, Status FROM Publisher WHERE Publisher_ID = '" + lstid.SelectedItem.ToString() + "'";
            SqlDataReader sdr;
            Profile pro = new Profile();
            sdr = pro.ReturnMDetails(sql);

            if (sdr.Read() == true)
            {
                txtPName1.Text = sdr["Publisher_Name"].ToString().Trim();
                //txtPYear.Text = sdr["Publishing_Year"].ToString().Trim();
                //txtPHouse.Text = sdr["Publishing_House"].ToString().Trim();
                //txtPEdition.Text = sdr["Publisher_Edition"].ToString().Trim();
                txtAdd1.Text = sdr["Address"].ToString().Trim();
                txtPhone1.Text = sdr["Phone"].ToString().Trim();
                txtFax1.Text = sdr["Fax"].ToString().Trim();
                txtemail1.Text = sdr["EMail"].ToString().Trim();
                chkStatus1.Checked = true;
                
            }
            sdr.Close();
            
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg1.Visible = true;
            lblEMsg1.Text = ex.Message;
        }
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        lblEMsg1.Visible = false;
        string[] Pub;
        Profile pro = new Profile();
        try
        {
            if (lstid.SelectedItem.Text == "")
            {
                lblEMsg1.Text = "Please Select Publisher ID";
                lblEMsg1.Visible = true;
            }
            else
            {
                Pub = new string[15];

                Pub[0] = lstid.Text;
                Pub[1] = txtPName1.Text;
                //Pub[2] = txtPYear.Text;
                //Pub[3] = txtPHouse.Text;
                //Pub[4] = txtPEdition.Text;
                Pub[5] = txtAdd1.Text;
                Pub[6] = txtPhone1.Text;
                Pub[7] = txtFax1.Text;
                Pub[8] = txtemail1.Text;
                if (chkStatus1.Checked == true)
                {
                    Pub[9] = "A";
                }
                else
                {
                    Pub[9] = "I";
                }

                bool status;
                status = pro.UpdatePublisher(Pub);

                if (status == false)
                {
                    lblEMsg1.Visible = true;
                    lblEMsg1.Text = "Publisher Updation Failed";
                }
                lblEMsg1.Visible = true;
                lblEMsg1.Text = "Publisher Updated.";
                lstid.Items.Clear();
                txtPName1.Text = "";
                //txtPYear.Text = "";
                //txtPHouse.Text = "";
                //txtPEdition.Text = "";
                txtAdd1.Text = "";
                txtPhone1.Text = "";
                txtFax1.Text = "";
                txtemail1.Text = "";
                chkStatus1.Checked = true;
                ReadPublisher();
                
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg1.Visible = true;
            lblEMsg1.Text = ex.Message;
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        lblEMsg1.Visible = false;
        Profile pro = new Profile();

        try
        {
            if (lstid.SelectedItem.ToString() == "")
            {
                lblEMsg1.Text = "Please Select Publisher ID";
                lblEMsg1.Visible = true;
            }
            else
            {
                bool status;
                status = pro.DeletePublisher(lstid.SelectedItem.ToString());

                if (status == false)
                {
                    lblEMsg1.Visible = true;
                    lblEMsg1.Text = "Publisher Deletion Failed";
                }
                lblEMsg1.Visible = true;
                lblEMsg1.Text = "Publisher Deleted.";

                lstid.Items.Clear();
                txtPName1.Text = "";
                //txtPYear.Text = "";
                //txtPHouse.Text = "";
                //txtPEdition.Text = "";
                txtAdd1.Text = "";
                txtPhone1.Text = "";
                txtFax1.Text = "";
                txtemail1.Text = "";
                chkStatus1.Checked = true;
                ReadPublisher();
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            lblEMsg1.Visible = true;
            lblEMsg1.Text = ex.Message;
        }
    }
}
