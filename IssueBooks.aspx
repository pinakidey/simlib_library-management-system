﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="IssueBooks.aspx.cs" Inherits="IssueBooks" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
  Response.Buffer = true;  
  Response.Expires = 0;
  Response.CacheControl = "no-cache";
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>SimLib --- Issue Books</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="Expires" content="0"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Pragma" content="no-cache"/>
<link href="tablestyle.css" rel="stylesheet" type="text/css" />
<link href="IssueBooks.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    function unavailable(source,args)
    {
    var quant=document.getElementById('<%=txtQuantity.Text%>');
        if(quant==0)
        {
        args.IsValid=true;
        }
        else
        {
        args.IsValid=false;
        }
    }
</script> 
    <style type="text/css">
        .style2
        {
            width: 149px;
        }
        .style3
        {
            width: 92px;
        }
        
       
        </style>
</head>

<body>
<form id="form3" runat="server">
	<div id="menu">
		<ul>
			<li class="active"><a href="Home.aspx" title="">Home</a></li>
			<li><a href="#" title="">Search Books</a></li>
			<li><a href="#" title="">Support</a></li>
			<li><a href="#" title="">About Us</a></li>
		</ul>
	</div>
	<div id="logo">
		<%--<h1><a href="#"><font size= "24">S</font>im<font size= "24">L</font>ib ---</a><span 
                class="style1"><a href="#"><font size= "6">Main Account</font></a></span>
        </h1>--%>
        <asp:Image ID="Header" runat="server" AlternateText="SimLib" Height="55px" 
            ImageAlign="Middle" ImageUrl="~/images/simlib (740 x 90).png" 
            BorderStyle="Groove" Width="733px" />
	</div>
<div id="content">
	<div id="sidebar">
		<div id="login" class="boxed">
			<h2 class="title" style="text-align: center">My Profile</h2>
			<div class="content">
				
					<fieldset>
                        <asp:Label ID="lblWelcome" runat="server" Text="Welcome" Font-Bold="True" 
                            Font-Names="Algerian" Font-Size="Large" ForeColor="#479AC6" Width="150px"></asp:Label>
					
                        <asp:Label ID="lblName" runat="server" Text="UserName" Font-Bold="False" 
                            Font-Names="Bernard MT Condensed" Font-Size="Small" ForeColor="#FFBD2E" 
                            Width="150px"></asp:Label>
                    <p>
                    </p>
					
                        <asp:HyperLink ID="HLogout" runat="server" Font-Bold="True" 
                            Font-Names="Bell MT" Font-Size="Small" ForeColor="Red" 
                            NavigateUrl="~/Default.aspx" Width="150px">Log Out</asp:HyperLink>
					
					<p>
					
                        &nbsp;</p>
					<p>
                        <asp:HyperLink ID="HEditProfile" runat="server" Font-Bold="True" 
                            Font-Names="Times New Roman" NavigateUrl="~/EditProfile.aspx" Target="_parent" 
                            ToolTip="Edit Your Profile" Width="150px">Edit Profile</asp:HyperLink>
                            </p>
                    
					
					</fieldset>
				
			</div>
		</div>
		<div id="updates" class="boxed">
			<h2 class="title" style="text-align: center">Utilities</h2>
			<div class="content">
				<ul>
					<li>
						<h3>
                            <asp:HyperLink ID="HSearch" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" NavigateUrl="~/Search.aspx" 
                                Width="150px" Target="_parent">Search Books</asp:HyperLink>
                        
                            <asp:HyperLink ID="HCheckedInBooks" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" NavigateUrl="~/CheckedIn.aspx" 
                                Target="_parent" Width="150px">CheckedIn Books</asp:HyperLink>
                        </h3>
						<p></p>
					</li>
                    <asp:Panel ID="Panel2" runat="server">
					<li>
						<h3 id="panel1">
                            <asp:HyperLink ID="HManBooks" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" NavigateUrl="~/ManageBooks.aspx" 
                                Target="_parent" Width="150px">Manage Books</asp:HyperLink>
                        
                            <asp:HyperLink ID="HManUsers" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" NavigateUrl="~/ManageUsers.aspx" 
                                Width="150px" Target="_parent">Manage Users</asp:HyperLink>    
                        
                            <asp:HyperLink ID="HManCat" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" 
                                NavigateUrl="~/ManageCategory.aspx" Target="_parent" Width="150px">Manage 
                            Category</asp:HyperLink>
                        
                            <asp:HyperLink ID="HManPub" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" 
                                NavigateUrl="~/ManagePublishers.aspx" Target="_parent" Width="150px">Manage 
                            Publishers</asp:HyperLink>
                            
                            <asp:HyperLink ID="HManAuthors" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" 
                                NavigateUrl="~/ManageAuthors.aspx" Target="_parent" Width="150px">Manage 
                            Authors</asp:HyperLink>
                        
                            <asp:HyperLink ID="HManSuppliers" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" 
                                NavigateUrl="~/ManageSuppliers.aspx" Target="_parent" Width="150px">Manage 
                            Suppliers</asp:HyperLink>
                        </h3>
						<p></p>
						<h3>
                            <asp:HyperLink ID="HIssue" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" NavigateUrl="~/IssueBooks.aspx" 
                                Target="_parent" Width="150px">Issue Books</asp:HyperLink>
						
                            <asp:HyperLink ID="HReturn" runat="server" Font-Bold="True" 
                                Font-Names="Times New Roman" Font-Size="Small" NavigateUrl="~/ReturnBooks.aspx" 
                                Target="_parent" Width="150px">Return Books</asp:HyperLink>
						</h3>
					</li>
					</asp:Panel>
				</ul>
			</div>
		</div>
	</div>
	<div id="main">
		<div id="welcome" class="post">
			<h2 class="title"
                style="font-family: Algerian; font-size: x-large; text-align:left">Issue Books
            </h2>
			<div class="story">
				<table id="gradient-style" summary="Registration Details">
                    <tbody>
    	                <tr>
        	                <td class="style3">Registration No.</td>
                            <td class="style2">
                                <asp:DropDownList ID="DropDownListRegistration" runat="server" Width="150px" 
                                    AutoPostBack="True" CausesValidation="True" 
                                    onselectedindexchanged="DropDownListRegistration_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Label ID="lblFullName" runat="server" Visible="False"></asp:Label>
                            <asp:RequiredFieldValidator ID="RegistrationIDRequired" runat="server" 
                                    ControlToValidate="DropDownListRegistration" Display="Dynamic" 
                                    ToolTip="Please Select Registration ID" ValidationGroup="Login1" 
                                    SetFocusOnError="True">Please 
                                Select Registration ID</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
        	                <td class="style3">Book No.</td>
                            <td class="style2">
                                <asp:DropDownList ID="LstBook" runat="server" AutoPostBack="True" 
                                    CausesValidation="True" Width="150px" 
                                    onselectedindexchanged="LstBook_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="BookNoRequired" runat="server" 
                                    ControlToValidate="LstBook" ToolTip="Please Select Book No." 
                                    ValidationGroup="Login1" Display="Dynamic" SetFocusOnError="True">Please Select Book No.</asp:RequiredFieldValidator></td>        	            
                        </tr>
                        <tr>
        	                <td class="style3">Book Title</td>
                            <td class="style2">
                                <asp:DropDownList ID="LstBTitle" runat="server" AutoPostBack="True" 
                                    CausesValidation="True" Width="150px" 
                                    onselectedindexchanged="LstBTitle_SelectedIndexChanged" 
                                    DataTextField="Book_Title" DataValueField="Book_Title">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:LibraryConnectionString1 %>" 
                                    SelectCommand="SELECT DISTINCT [Book_Title] FROM [Book_Master]">
                                </asp:SqlDataSource>
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="TitleRequired" runat="server" ControlToValidate="LstBTitle" 
                                    ToolTip="Please Select Book Title" ValidationGroup="Login1" 
                                    Display="Dynamic" SetFocusOnError="True">Please Select Book Title</asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
        	                <td class="style3">Author</td>
                            <td class="style2">
                                <asp:TextBox ID="txtAuthor" runat="server" MaxLength="30" 
                                    Width="150px" ReadOnly="True"></asp:TextBox></td>
                            <td></td>
                        </tr>
                        <tr>
        	                <td class="style3">Publisher</td>
                            <td class="style2">
                                <asp:TextBox ID="txtPub" runat="server" MaxLength="30" Width="150px" 
                                    ReadOnly="True"></asp:TextBox></td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
        	                <td class="style3">Category</td>
                            <td class="style2">
                                <asp:TextBox ID="txtCat" runat="server" MaxLength="30" Width="150px" 
                                    ReadOnly="True"></asp:TextBox></td>
                            <td></td>
                        </tr>
                        <tr>
        	                <td class="style3">Supplier</td>
                            <td class="style2">
                                <asp:TextBox ID="txtSup" runat="server" MaxLength="30" Width="150px" 
                                    ReadOnly="True"></asp:TextBox></td>
                            <td></td>
                        </tr>
                        <tr>
        	                <td class="style3">Available Quantity</td>
                            <td class="style2">
                                <asp:TextBox ID="txtQuantity" runat="server" Width="150px" ReadOnly="True"></asp:TextBox></td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
        	                <td class="style3">Price</td>
                            <td class="style2">
                                <asp:TextBox ID="txtPrice" runat="server" MaxLength="30" Width="150px" 
                                    ReadOnly="True"></asp:TextBox>
                                    </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
        	                <td class="style3">Issue Date</td>
                            <td class="style2">
                                <asp:TextBox ID="txtIDate" runat="server" MaxLength="15" 
                                    ValidationGroup="Login1" Width="150" ReadOnly="True"></asp:TextBox></td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
        	                <td class="style3">Return Date</td>
                            <td class="style2">
                                <asp:TextBox ID="txtRDate" runat="server" MaxLength="15" Width="115px" 
                                    ReadOnly="True"></asp:TextBox>
                                &nbsp;<asp:Button ID="btnCal" runat="server" Text="..." 
                                    Height="20px" Width="20px" Font-Bold="True" ForeColor="#003399" 
                                    onclick="btnCal_Click" UseSubmitBehavior="False" />
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="ReturnDateRequired" runat="server" 
                                    ControlToValidate="txtRDate" Display="Dynamic" ValidationGroup="Login1">Please Select Return Date</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="DateCompare" runat="server" 
                                    ControlToCompare="txtIDate" ControlToValidate="txtRDate" Display="Dynamic" 
                                    Operator="LessThanEqual" ToolTip="Select Appropriate Date" 
                                    ValidationGroup="Login1">Date Already Past</asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
        	                <td class="style3"></td>
                            <td class="style2" align="center">
                                <asp:Calendar ID="Calendar1" runat="server" CellPadding="0" 
                                    DayNameFormat="FirstLetter" Font-Size="X-Small" Visible="False" 
                                    Width="150px" onselectionchanged="Calendar1_SelectionChanged" 
                                    ToolTip="Select Return Date" Caption="Calendar">
                                    <WeekendDayStyle BackColor="#FFCCFF" Font-Bold="True" ForeColor="#FF3300" />
                                    <TodayDayStyle BorderColor="#FF3300" BorderStyle="Solid" BorderWidth="2px" />
                                </asp:Calendar>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
        	                <td class="style3"></td>
                            <td class="style2" align="center">
                                <b></b>
                                <asp:Label
                                        ID="lblEMsg" runat="server" ForeColor="Red" Visible="False" 
                                    Font-Size="Larger"></asp:Label></td>
                            <td></td>
                        </tr>
                        <tr>
        	                <td class="style3"></td>
                            <td class="style2" align="center">
                                <asp:Button ID="btnIssue" OnClientClick="return confirm('The Book Will Be Issued To The Member!');" runat="server" Text="Issue" ToolTip="Issue Book" 
                                    ValidationGroup="Login1" onclick="btnIssue_Click" Width="75px" />
                            </td>
                            <td align="center">
                                <asp:Button ID="btnClear" runat="server" Text="Clear" 
                                    onclick="btnClear_Click" Width="75px" /></td>
                        </tr>
                        
                    </tbody>
                </table>
            </div>
		</div>
		<div id="example" class="post">
		</div>
	</div>
</div>

<div id="footer">
	<p id="legal">Copyright © 2010. All Rights Reserved. <%--Designed by <a href="http://www.pinakidey.emurse.com/">
        PINAKI DEY</a>.--%></p>
	<p id="links"><a href="#">Privacy Policy</a> | <a href="license.txt">Terms of Use</a></p>
</div>
</form>
</body>
</html>
